import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [vue()],
  base: "/tabletop/",
  publicDir: "public",
  build: {
    outDir: "dist/tabletop",
    emptyOutDir: true,
    rollupOptions: {
      output: {
        manualChunks: {
          "vue": ["vue"],
          "peerjs": ["peerjs"],
          "@phosphor-icons/vue": ["@phosphor-icons/vue"],
        },
      },
    },
  },
});
