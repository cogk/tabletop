import Peer from "peerjs";
import { Bus } from "../utils/bus";
import { makeId } from "../utils/random";
import { Auth } from "./auth";
import { serialize } from "./serialize";
import { gameBus } from "../bus";
import { EventAccumulator } from "../utils/eventAccumulate";

const accumulatorInstances = {};

const accumulators = [
  // {
  //   filter: (event) => event.type === "game:cursor:move",
  //   keyer: (event) => `${event.type}:${event.author}`,
  //   reducer: EventAccumulator.reduceTakeLast,
  //   delay: 32,
  // },
  {
    filter: (event) => event.type === "game:item:move",
    keyer: (event) => `${event.type}:${event.author}:${event.iid}`,
    reducer: EventAccumulator.reduceTakeLast,
    delay: 32,
  },
];

function accumulate(event, next) {
  const accumulator = accumulators.find((a) => a.filter(event));
  if (accumulator) {
    const key = accumulator.keyer(event);
    if (key.includes("undefined")) {
      console.warn("suspicious accumulator key", event);
    }
    if (!(key in accumulatorInstances)) {
      console.log("creating accumulator", key);
      accumulatorInstances[key] = EventAccumulator.make(accumulator.reducer, next, accumulator.delay);
    }
    accumulatorInstances[key](event);
  } else {
    next(event);
  }
}

export class P2P {
  /**
   * @param {string} roomId
   * @param {import("../global/app").App} app
   */
  static async becomeHost(roomId, app) {
    const host = new P2PHost(roomId, app);
    await host.ready;

    const client = new P2PClient(roomId, app);
    await client.ready;
    return { host, client };
  }

  /**
   * @param {string} roomId
   * @param {import("../global/app").App} app
   */
  static async becomeClient(roomId, app) {
    const client = new P2PClient(roomId, app);
    await client.ready;
    return client;
  }
}

export class P2PClient {
  /**
   * @param {string} roomId
   * @param {import("../global/app").App} app
   */
  constructor(roomId, app) {
    this.app = app;
    this.changeRoom(roomId);
  }

  /** @returns {import("../items/GameState").GameState} state */
  get state() {
    return this.app.state;
  }

  toString() {
    return `P2PClient(${this.peer?.id ?? "[offline]"} -> ${this.roomId ?? "[local]"})`;
  }

  changeRoom(newRoomId) {
    this.destroy();
    this.roomId = newRoomId;
    this.make();
  }

  make() {
    this.bus = new Bus();
    this.peer = new Peer();
    this.token = "[unauthenticated]";

    this.ready = new Promise((resolve, reject) => {
      this.bus.on("net:intro:welcome", () => resolve());
      this.peer.on("error", reject);
    });

    this.peer.on("error", (error) => {
      console.error(error);
      // gameBus.emitEvent({
      //   type: "net:error",
      //   data: { error },
      // });
    });

    this.peer.on("open", (id) => {
      console.log("my id", id);
      this.listen();
    });
  }

  destroy() {
    this.connection?.close();
    this.peer?.destroy();
    this._unlistenGameEvents?.();
    this.bus = new Bus();
    this.peer = null;
    this.token = "[destroyed]";
  }

  listen() {
    this.connection = this.peer.connect(this.roomId, {
      reliable: true,
    });
    this.connection.on("data", (event) => {
      this.onReceive(event);
    });
    this.connection.on("close", () => {
      console.log("client/close");
    });

    this._unlistenGameEvents = gameBus.onRegex(/^game:/, (event) => {
      if (event.__remote) return; // don't echo remote events
      accumulate(event, this.send.bind(this));
      // this.send(event); // send local events
    });
  }

  async onReceive(event) {
    event.__remote = true;
    this.bus.emitEvent(event);

    // const { __source: src, type, data } = event;
    // if (src === this.peer.id) return console.warn("received event from self", event);
    const { type, data } = event;

    if (type === "net:intro:challenge") {
      this.introConfirm(event);
    }
    else if (type === "net:intro:welcome") {
      gameBus.emitEvent(event);
      // this.state.takeFrom(data.state);
    }
    else if (type === "net:joined") {
      console.info("A new player has joined the game:", data);
      gameBus.emitEvent(event);
      // this.state.addPlayer(data);
    }
    else if (type === "net:ping") {
      console.info("ping!");
      this.send({ type: "net:pong" });
    }
    else if (type === "net:pong") {
      console.info("pong!");
    }
    else if (type.startsWith("net:")) {
      console.warn("unknown net event", type);
    }
    else if (type.startsWith("game:")) {
      const msg = Auth.checkP2PIncomingEvent(this.state, event);
      if (msg) {
        console.error("hackers!!", msg, event);
      } else {
        gameBus.emitEvent(event);
      }
    } else {
      console.warn("unknown net event", type);
    }
  }

  introConfirm(event) {
    if (event.type !== "net:intro:challenge") return console.warn("expected net:intro:challenge");
    this.token = event.data.yourToken;
    this.send({
      type: "net:intro:confirm",
      data: {
        uid: this.app.player.uid,
        displayName: this.app.player.displayName,
        color: this.app.player.color,
      },
    });
  }

  ping() {
    this.send({ type: "net:ping" });
  }

  send(event) {
    if (event.author && event.author !== this.app.player.uid) {
      throw new Error("Cannot send event as another user");
    }
    event = this.sign(event);
    this.connection?.send(event);
  }

  sign(event) {
    const out = { ...event };
    // out.__token = this.token;
    out.author = this.app.player.uid;
    return out;
  }
}

export class P2PHost {
  /**
   * @param {string} roomId
   * @param {import("../global/app").App} app
   */
  constructor(roomId, app) {
    this.app = app;
    this.roomId = roomId;

    this.connections = [];
    this.mapUidToToken = {};
    this.mapUidToUserInfo = {};

    this.room = new Peer(roomId);
    this.room.on("error", (x) => console.error(x));

    this.room.on("open", (id) => {
      console.info("room id:", id);
      this.listen();
    });

    this.ready = new Promise((resolve) => {
      this.room.on("open", () => resolve());
    });
  }

  /** @returns {import("../items/GameState").GameState} state */
  get state() {
    return this.app.state;
  }

  destroy() {
    this.connections.forEach((c) => c.close());
    this.mapUidToToken = {};
    this.mapUidToUserInfo = {};
    this.room?.destroy();
  }

  listen() {
    this.room.on("connection", (conn) => {
      conn.on("open", () => {
        this.introSendChallenge(conn);
      });
    });
  }

  introSendChallenge(conn) {
    this.connections.push(conn);
    this.mapUidToToken[conn.connectionId] = makeId("authtoken_");

    conn.on("data", (event) => {
      this.onReceive(event, conn);
    });
    conn.on("close", () => {
      this.connections = this.connections.filter((c) => c !== conn);
    });
    conn.on("error", (...args) => {
      console.error(...args);
      this.connections = this.connections.filter((c) => c !== conn);
    });

    conn.send({
      author: "[host]",
      type: "net:intro:challenge",
      data: {
        yourToken: this.mapUidToToken[conn.connectionId],
      },
    });
  }

  async introWelcome(event, conn) {
    if (event.type !== "net:intro:confirm") return console.warn("expected net:intro:confirm");
    const { data } = event;

    const userInfo = {
      uid: data.uid,
      displayName: data.displayName ?? makeId("Anon_"),
      color: data.color ?? `hsl(${Math.random() * 360}, 100%, 50%)`,
    };
    this.mapUidToUserInfo[conn.connectionId] = userInfo;
    this.state.addPlayer(userInfo);

    this.broadcast({
      author: "[host]",
      type: "net:joined",
      data: { ...userInfo },
    }, conn);

    conn.send({
      author: "[host]",
      type: "net:intro:welcome",
      data: await this.getIntroData(conn),
    });
  }

  async getIntroData() {
    const data = { state: await serialize(this.state) };
    return data;
  }

  onReceive(event, conn) {
    const ok = this.authenticate(event, conn);
    if (!ok) return console.error("unauthenticated event", event);

    if (event.type === "net:intro:confirm") {
      return this.introWelcome(event, conn);
    }

    if (event.type.startsWith("game:") || event.type === "net:ping" || event.type === "net:pong") {
      return this.broadcast(event, conn);
    }

    console.error("unhandled event", event);
  }

  broadcast(originalEvent, sourceConnection = null) {
    if (originalEvent.type.startsWith("net:intro:")) throw new Error("should not broadcast net:intro:* events");

    const event = this.sanitize(originalEvent);
    // event.__source = sourceConnection?.peer ?? "[host]";
    for (const conn of this.connections) {
      if (conn === sourceConnection) continue; // don't send back to sender (if any)
      conn.send(event);
    }
  }

  sanitize(event) {
    const sanitized = { ...event };
    delete sanitized.__token;
    return sanitized;
  }

  authenticate(event, conn) { // eslint-disable-line no-unused-vars
    return true;
    // if (event.__token !== this.mapUidToToken[conn.connectionId]) {
    //   console.warn("invalid token");
    //   return false;
    // }
    // return true;
  }
}
