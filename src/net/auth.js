/**
 * @typedef {object} GameEvent
 * @property {string} iid
 * @property {string} author
 * @property {string} type
 * @property {object} data
 */

export class Auth {
  static _checkEvent(event) {
    // if (!event.iid) throw new Error("Event must have an Item ID (`iid`)");
    // if (!event.author) throw new Error("Event must have an Author (`author`)");
    if (!event.type) throw new Error("Event must have a Type (`type`)");
    // if (!event.data) throw new Error("Event must have a Data property (`data`)");
  }

  /**
   * @param {import("../items/GameState").GameState} state
   * @param {GameEvent} event
   * @returns {boolean}
   */
  static check(state, event) {
    return "";
    return Auth.checkAsUser(state, event, state.app.player.uid);
  }

  /**
   * @param {import("../items/GameState").GameState} state
   * @param {GameEvent} event
   * @returns {boolean}
   */
  static checkP2PIncomingEvent(state, event) {
    return "";
    return Auth.checkAsUser(state, event, event.author);
  }

  /**
   * @param {import("../items/GameState").GameState} state
   * @param {GameEvent} event
   * @returns {boolean}
   */
  static checkAsUser(state, event, euid) {
    Auth._checkEvent(event);

    if (event.type === "[invalid]") return "Invalid event type";

    if (event.iid) {
      const item = state.app.getItem(event.iid);
      const c = Auth.checkItem(item, euid);
      if (c) return c;
    } else if (event.data?.iids) {
      const items = state.app.getItems(event.data.iids);
      const cs = items.map(item => Auth.checkItem(item, euid));
      const c = cs.filter(Boolean).join("; ");
      if (c) return c;
    } else if (event.type === "game:spawn") {
      if (!Array.isArray(event.data.items)) {
        return "game:spawn requires an array of items";
      }
      if (event.data.items.some(item => !item.type)) {
        return "game:spawn requires all items to have a `type`";
      }
      if (event.data.items.some(item => !item.iid)) {
        return "game:spawn requires all items to have a `iid`";
      }
      return null; // TODO: check if spawn is allowed with a permission system
    } else {
      console.warn("auth: unhandled event", event);
    }

    return null;
  }

  static checkItem(item, euid) {
    if (!item) return "Item does not exist";
    if (!item.public && item.owners?.length && !item.owners?.includes(euid)) {
      return `Item is not public and “${euid}” is not an owner: Item “${item.iid}” is owned by ${item.owners.join(", ")}`;
    }
  }
}
