import { serialize, hydrate } from "./serialize.js";

class AssertionError extends Error {
  constructor(msg, context = null) {
    super(msg);
    this.name = "AssertionError";
    if (context?.name) {
      this.name += ` (in test '${context.name}')`;
    }
  }
}

function assert(cond, msg, context = null) {
  if (!cond) throw new AssertionError(msg, context);
}

const allErrors = [];
const ESC_CLEAR_UP = "\x1b[1A\x1b[2K";
const SYMBOL_CHECK = "\u2713";
const SYMBOL_CROSS = "\u2718";
const SYMBOL_PENDING = "\u2026";

function test(name, fn) {
  console.log(`\x1b[33m${SYMBOL_PENDING} ${name}\x1b[0m`);
  try {
    fn((cond, msg) => assert(cond, msg, { name }));
    console.log(`${ESC_CLEAR_UP}\x1b[32;2m${SYMBOL_CHECK} ${name}\x1b[0m`);
  } catch (e) {
    console.error(`${ESC_CLEAR_UP}\x1b[31;1m${SYMBOL_CROSS} ${name}\x1b[0m`);
    allErrors.push(e);
  }
}

test("basic hydration/serialization", (assert) => {
  const item = hydrate({ type: "token" });
  const serialized = serialize(item);
  assert(item.getType() === serialized.type, "Expected types to match");
});

test("multiple cycles of serialization", (assert) => {
  // Resists multiple de/serialization
  let x = { type: "token", color: "#123456" };
  for (let i = 0; i < 100; i++) {
    x = hydrate(x);
    x = serialize(x);
  }
  assert(x.type === "token", "Expected type to be 'token'");
  assert(x.color === "#123456", "Expected color to be '#123456'");
});

// test("fail", (assert) => assert(false, "Expected false to be true"));

if (allErrors.length) {
  console.error("\n\x1b[31;1mTest failures:\x1b[0m");
  for (const e of allErrors) {
    console.error(e);
  }
  process.exit(1);
}
