import { Serializable } from "../items/Serializable";
import { knownTypes } from "../items/index";

// Generic (de)serialization utils
function passthrough2(x, iter) {
  if (Array.isArray(x)) {
    return x.map((x) => iter(x));
  }
  return x;
}

function isDeserializable(x) {
  return x && typeof x === "object" && typeof x.type === "string";
}

/**
 * @param {import("../items/Serializable").Serializable} item
 * @returns {object} serialized
 */
export function serialize(item) {
  if (!(item instanceof Serializable)) {
    return passthrough2(item, serialize);
  }

  const dep = { serializer: serialize };
  const serialized = item.serialize(dep);
  Object.freeze(serialized);

  if (serialized.type !== item.getType()) {
    console.error({ serialized, item });
    return alert("Expected types to match");
  }
  // if (!serialized.iid) {
  //   console.error("serialize: missing iid", { serialized, item });
  // }
  return serialized;
}

/**
 * @param {object} serialized
 * @returns {import("../items/Serializable").Serializable} item
 */
export function hydrate(serialized) {
  if (!isDeserializable(serialized)) {
    return passthrough2(serialized, hydrate);
  }

  const dep = { serialized, hydrator: hydrate };
  const type = serialized.type;
  const Class = knownTypes[type];

  if (!Class) {
    console.warn(`deserialize: unknown type '${type}'`);
    return passthrough2(serialized, hydrate);
  }

  /** @type {import("../items/Serializable").Serializable} */
  const item = new Class();
  item.hydrate(dep);
  return item;
}


/**
 * Hydrate then serialize an item to ensure its type, iid, etc. are defined.
 * Useful to ensure that a game:spawn event has the same effect everywhere in the network.
 * @param {object} serialized
 * @returns {object} reserialized
 */
export function hydroserialize(serialized) {
  return serialize(hydrate(serialized));
}
