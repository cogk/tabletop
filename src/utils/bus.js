// https://stackoverflow.com/questions/63471824/vue-js-3-event-bus

export class Bus {
  constructor(name = null) {
    /** @type {string} */
    this.name = name ?? "bus";

    /** @type {Record<string, Function[]>} */
    this.events = {};

    /** @type {Array<{ re: RegExp, fn: Function }>} */
    this.eventsRegex = [];
  }

  onRegex(re, fn) {
    if (!(re instanceof RegExp)) throw new Error("re must be a RegExp");
    const o = { re, fn };
    this.eventsRegex.push(o);
    return () => {
      this.eventsRegex = this.eventsRegex.filter((x) => x !== o);
    };
  }

  on(eventName, fn) {
    if (window.DEBUG === "listen") {
      console.warn(`%c${this.name} %c${eventName} %cLISTEN`, "color:#d0d", "color:#dd0;font-weight:bold;", "color:#f00;font-style:italic;");
    }
    if (typeof eventName !== "string") throw new Error("eventName must be a string");
    this.events[eventName] = this.events[eventName] || [];
    this.events[eventName].push(fn);
    return () => {
      this.events[eventName] = this.events[eventName].filter((x) => x !== fn);
    };
  }

  off(eventName, fn) {
    if (this.events[eventName]) {
      for (let i = 0; i < this.events[eventName].length; i++) {
        if (this.events[eventName][i] === fn) {
          this.events[eventName].splice(i, 1);
          break;
        }
      }
    }
  }

  emit(eventName, event) {
    event = event || {};
    event.type = eventName;
    this.emitEvent(event);
  }

  /**
   * @param {import("./eventFactory").GameEvent} event
   */
  emitEvent(event) {
    const eventName = event.type;
    if (typeof eventName !== "string") throw new Error("eventName must be a string");

    if (window.DEBUG === 1) {
      if (!eventName.match(/:move|:refresh|:hover|ui:interact/)) {
        console.warn(`%c${this.name} %c${eventName}`, "color:#d0d", "color:#dd0;font-weight:bold;", event);
      }
    }
    // if (window.buslog) {
    //   const li = document.createElement("li");
    //   const basicJson = basicJsonify(event);
    //   li.innerText = `${this.name} ${eventName} ${JSON.stringify(basicJson)}`.replaceAll("\"", "");
    //   window.buslog.append(li);
    //   window.buslog.scrollTop = window.buslog.scrollHeight;
    // }
    let used = false;
    if (this.events[eventName]) {
      this.events[eventName].forEach(function (fn) {
        fn(event, eventName);
        used = true;
      });
    }
    this.eventsRegex.forEach(({ re, fn }) => {
      if (re.test(eventName)) {
        fn(event, eventName);
        used = true;
      }
    });

    if (!used && window.DEBUG === "unused") {
      console.warn(`%c${this.name} %c${eventName} %cunused`, "color:#d0d", "color:#dd0;font-weight:bold;", "color:#d00;font-weight:bold;", event);
    }

    // function basicJsonify(data) {
    //   const basicJson = {};
    //   for (const key in data) {
    //     if (data[key]?.constructor === Object) {
    //       basicJson[key] = basicJsonify(data[key]);
    //     } else {
    //       basicJson[key] = String(data[key]);
    //     }
    //   }
    //   return basicJson;
    // }
    // if (eventName !== "*") {
    //   this.emit("*", event);
    // }
  }
}
