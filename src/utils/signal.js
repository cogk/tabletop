import { onMounted, onUnmounted, ref } from "vue";

export class Signal extends Function {
  constructor() {
    super();
    this._watchers = [];

    return new Proxy(this, {
      apply: (target, _thisArg, args) => target.trigger(...args),
    });
  }

  watch(callback) {
    if (typeof callback !== "function") throw new Error("Callback must be a function");
    this._watchers.push(callback);
    return () => {
      this._watchers = this._watchers.filter((cb) => cb !== callback);
    };
  }

  trigger() {
    this._watchers.forEach((cb) => cb());
  }

  use() {
    const r = ref(0);
    let off;
    onMounted(() => {
      off = this.watch(() => {
        r.value++;
      });
    });
    onUnmounted(() => {
      off?.();
      r.value++;
    });
    return r;
  }
}
