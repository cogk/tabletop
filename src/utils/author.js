export function getAuthor(event) {
  if (event.author === "[host]") {
    console.error(event);
    throw new Error("You shouldn't ask about the author of an event that can come from [host]!");
  }

  if (event.author) {
    return event.author;
  }

  if (event.__remote) {
    console.error(event);
    throw new Error("Remote event is missing author!");
  }

  return this.app.player.uid;
}
