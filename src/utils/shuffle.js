import { mulberry32 } from "./random";

const required = (name) => { throw new Error(`required: ${name}`); };

/** @param {T[]} array */
export function fisherYatesInPlace(array, seed = required("seed")) {
  const rng = mulberry32(seed);
  for (let tmp, i = array.length - 1; i > 0; i--) {
    const rand = Math.floor(rng() * (i + 1));
    tmp = array[i];
    array[i] = array[rand];
    array[rand] = tmp;
  }
}

// const newspaperSpinning = [
//   { transform: "rotate(0) scale(1)" },
//   { transform: "rotate(360deg) scale(0)" },
// ];

// const newspaperTiming = {
//   duration: 2000,
//   iterations: 1,
// };

// const newspaper = document.querySelector(".newspaper");

// newspaper.addEventListener("click", () => {
//   newspaper.animate(newspaperSpinning, newspaperTiming);
// });
