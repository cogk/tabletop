import { PhUser } from "@phosphor-icons/vue";

export function getPlayerList(players, myUid) {
  const isMe = (player) => player.uid === myUid;
  const makeItem = (player) => {
    const myself = isMe(player);
    let label = player.displayName;

    if (myself) {
      label = `${label} (vous)`;
    }

    return {
      label,
      isMe: myself,
      icon: PhUser,
      color: player.color,
      player: player,
    };
  };

  let list = players.slice();

  // Sort by name, with me always first
  list.sort((a, b) => {
    if (isMe(a)) return -1;
    if (isMe(b)) return 1;
    return a.displayName.localeCompare(b.displayName);
  });

  // Map to menu items
  list = list.map(makeItem);

  return list;
}
