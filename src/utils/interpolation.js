const hermite = (target, t, pos1, po2, vel1, vel2) => {
  const t2 = t * t;
  const t3 = t * t * t;
  const a = 2 * t3 - 3 * t2 + 1;
  const b = -2 * t3 + 3 * t2;
  const c = t3 - 2 * t2 + t;
  const d = t3 - t2;

  target.copy(pos1.multiplyScalar(a));
  target.add(po2.multiplyScalar(b));
  target.add(vel1.multiplyScalar(c));
  target.add(vel2.multiplyScalar(d));
};
