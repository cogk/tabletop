export function getItemIdFromElement(element) {
  return element.dataset.iid || element.getAttribute("iid") || element.id || null;
}

export function getElementItemXY(element) {
  return {
    x: Number(element.getAttribute("data-x") || 0),
    y: Number(element.getAttribute("data-y") || 0),
  };
}

export function setTransform(el, x, y, opts = {}) {
  if (!el || x === undefined || y === undefined) return;
  const { scale = 1 } = opts;
  let str;
  if (scale === 1 || scale === null) {
    str = `translate(${x}, ${y})`;
  } else {
    str = `translate(${x}, ${y}) scale(${scale})`;
  }
  el.setAttribute("transform", str);
  el.setAttribute("data-x", x);
  el.setAttribute("data-y", y);
}
