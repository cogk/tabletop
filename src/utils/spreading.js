import { mulberry32 } from "./random";
import { fisherYatesInPlace } from "./shuffle";

const STACK_SPREAD = 24;
const LINE_SPREAD = 150;

export function reorder_mut(itemsOrPositions) {
  let now = 0;
  for (const item of itemsOrPositions) {
    const z = item.z;
    // eslint-disable-next-line eqeqeq
    if (z != null && z > now) {
      now = z;
    }
  }

  for (let index = 0; index < itemsOrPositions.length; index++) {
    itemsOrPositions[index].z = now + index; // lift everything
  }
}

export function offset_mut(items, translation) {
  for (const item of items) {
    item.x ??= 0;
    item.y ??= 0;
    item.x += translation.x;
    item.y += translation.y;
  }
}

export function disc(center, items, { seed }) {
  const rng = mulberry32(seed);
  const SPREAD = items.length * 10;
  const spread = (item) => {
    const dist = Math.sqrt(rng()) * SPREAD;
    const angle = rng() * Math.PI * 2;
    const x = center.x + Math.cos(angle) * dist;
    const y = center.y + Math.sin(angle) * dist;
    return { ...item, x, y };
  };
  return reordered(items.map(spread));
}

export function circle(center, items) {
  const SPREAD = Math.sqrt(items.length) * 100;
  const spread = (item, index) => {
    const dist = SPREAD;
    const angle = (index / items.length) * Math.PI * 2;
    const x = center.x + Math.cos(angle) * dist;
    const y = center.y + Math.sin(angle) * dist;
    return { ...item, x, y };
  };
  return reordered(items.map(spread));
}

export function horizontal(center, items) {
  const N = items.length;
  const spread = (item, index) => {
    const x = center.x + (index - (N - 1) / 2) * LINE_SPREAD;
    const y = center.y;
    return { ...item, x, y };
  };
  return reordered(items.map(spread));
}

export function vertical(center, items) {
  const N = items.length;
  const spread = (item, index) => {
    const x = center.x;
    const y = center.y + (index - (N - 1) / 2) * LINE_SPREAD;
    return { ...item, x, y };
  };
  return reordered(items.map(spread));
}

export function stackHorizontal(center, items) {
  const N = items.length;
  const spread = (item, index) => {
    const x = center.x + (index - (N - 1) / 2) * STACK_SPREAD;
    const y = center.y;
    return { ...item, x, y };
  };
  return reordered(items.map(spread));
}

export function stackVertical(center, items) {
  const N = items.length;
  const spread = (item, index) => {
    const x = center.x;
    const y = center.y + (index - (N - 1) / 2) * STACK_SPREAD;
    return { ...item, x, y };
  };
  return reordered(items.map(spread));
}

export function stack(center, items) {
  return reordered(items.map((item) => ({ ...item, ...center })));
}

export function shuffle(_center, items, { seed }) {
  let positions = items.map(({ x, y }) => ({ x, y }));
  positions = reordered(positions);
  fisherYatesInPlace(positions, seed);
  return positions;
}

export const deal = (count) => (center, items) => {
  const spread = (item, index) => {
    const stackIndex = index % count;
    const x = center.x + (stackIndex - (count - 1) / 2) * LINE_SPREAD;
    const y = center.y;
    return { ...item, x, y };
  };
  return reordered(items.map(spread));
};

export function findCenterOfItems(items) {
  const x = items.reduce((acc, item) => acc + item.x, 0) / items.length;
  const y = items.reduce((acc, item) => acc + item.y, 0) / items.length;
  return { x, y };
}

export function applySpreadOnIds(app, iids, spreadFn, event) {
  if (spreadFn === deal) {
    spreadFn = spreadFn(app.state.players.length || 1);
  }

  const opts = event?.data ?? {};
  const items = app.getItems(iids);
  items.sort((a, b) => (a.z ?? 0) - (b.z ?? 0));
  iids = items.map((item) => item.iid);

  const center = findCenterOfItems(items);
  const positions = spreadFn(center, items, opts);

  app.emitEventIfAllowed({
    type: "game:items:movemany",
    data: { iids, positions },
  });
}

function reordered(positions) {
  reorder_mut(positions);
  return positions;
}
