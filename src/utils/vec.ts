const assert = (cond: boolean, msg: string) => {
  if (!cond) {
    debugger;
    throw new Error(msg);
  }
};

export interface Vec2 {
  x: number;
  y: number;
}

export interface Delta2 {
  dx: number;
  dy: number;
}

export interface Dim2 {
  w: number;
  h: number;
}

export interface Rect2 extends Vec2, Dim2 { }

export function vec2_round_mut(vec: Vec2) {
  vec.x = Math.round(vec.x);
  vec.y = Math.round(vec.y);
  return vec;
}

export function isInBounds(vec: Vec2, bounds: Rect2) {
  assert(!isNaN(vec.x) && !isNaN(vec.y), "vec is not a Vec2");
  assert(!isNaN(bounds.x) && !isNaN(bounds.y) && !isNaN(bounds.w) && !isNaN(bounds.h), "bounds is not a Rect2");
  return vec.x >= bounds.x && vec.x < bounds.x + bounds.w && vec.y >= bounds.y && vec.y < bounds.y + bounds.h;
}

export function vecSum(...vs: Vec2[]) {
  const out = { x: 0, y: 0 };
  for (const v of vs) {
    out.x += v.x;
    out.y += v.y;
  }
  return out;
}

export function vecDiff(a: Vec2, b: Vec2) {
  return { x: a.x - b.x, y: a.y - b.y };
}
