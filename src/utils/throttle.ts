import t from "lodash.throttle";

export function throttle<F extends Parameters<typeof t>[0]>(callback: F, delay = 1000, leading = false, trailing = false) {
  return t(callback, delay, { leading, trailing });
}
