export function map<T, U>(array: T[], fn: (item: T, index: number) => U) {
  const out = [];
  for (let index = 0; index < array.length; index++) {
    out.push(fn(array[index], index));
  }
  return out;
}

export function mapobj<T>(obj: T, fn: (key: keyof T, value: T[keyof T]) => unknown) {
  type O = Record<keyof T, unknown>;
  const out: O = {} as O;
  for (const key in obj) {
    out[key] = fn(key, obj[key]);
  }
  return out;
}

// https://dev.to/chrismilson/zip-iterator-in-typescript-ldm
type Iterableify<T> = { [K in keyof T]: Iterable<T[K]> }
export function* zip<T extends Array<unknown>>(
  ...toZip: Iterableify<T>
): Generator<T> {
  // Get iterators for all of the iterables.
  const iterators = toZip.map(i => i[Symbol.iterator]());

  while (true) {
    // Advance all of the iterators.
    const results = iterators.map(i => i.next());

    // If any of the iterators are done, we should stop.
    if (results.some(({ done }) => done)) {
      break;
    }

    // We can assert the yield type, since we know none
    // of the iterators are done.
    yield results.map(({ value }) => value) as T;
  }
}


// export function propsum<T>(dims: T[], key: keyof T, dflt = 0) {
//   let out = 0;
//   for (const dim of dims) {
//     const v = (dim[key] ?? dflt) as number;
//     out += v;
//   }
//   return out;
// }

// export function* pick<T>(array: T[], key: keyof T) {
//   for (const item of array) {
//     yield item[key];
//   }
// }
