/**
 * @typedef {object} GameEvent
 * @property {string} author
 * @property {string} type
 * @property {string?} iid
 * @property {object} data
 */

import { hydrate, serialize } from "../net/serialize";

export class EventFactory {
  /**
   *
   * @param {import("./app").App} app
   * @param {import("./items/GameState").GameState} state
   */
  constructor(app, state) {
    this.app = app;
    this.state = state;
  }

  /**
   * @param {Partial<GameEvent>} event
   * @returns {GameEvent}
   */
  E(event) {
    return {
      ...event,
      iid: event.iid ?? this.state.id,
      author: event.author ?? this.app.player.uid,
      type: event.type ?? "[invalid]",
      data: event.data ?? {},
    };
  }

  /**
   * @param {string} iid
   * @param {{ x: number, y: number, z?: number }} to
   * @returns {GameEvent}
   */
  move(iid, to) {
    return this.E({
      iid,
      type: "game:item:move",
      data: { x: to.x, y: to.y, z: to.z ?? undefined },
    });
  }

  /**
   * @param {string[]} iids
   * @param {{ x: number, y: number, z?: number }[]} positions
   * @returns {GameEvent}
   */
  movemany(iids, positions) {
    return this.E({
      type: "game:items:movemany",
      data: {
        iids,
        positions,
      },
    });
  }

  lift(iid) {
    return this.E({ iid, type: "game:item:lift", data: { z: Date.now() } });
  }

  flipShow(iid) {
    return this.E({ iid, type: "game:item:flipShow" });
  }

  flipHide(iid) {
    return this.E({ iid, type: "game:item:flipHide" });
  }

  spawn(item) {
    // hydrate -> serialize to initialize iid and fields
    const serialized = serialize(hydrate(item));
    return this.E({ type: "game:spawn", data: { item: serialized } });
  }
}
