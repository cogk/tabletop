import { throttle } from "./throttle";

export class EventAccumulator {
  /**
   * @param {(agg: T | null, it: T) => T | null} reducer
   * @param {number} delay
   */
  constructor(reducer, callback, delay) {
    this.reducer = reducer;
    this.callback = callback;
    this.delay = delay;
    this.events = [];
    this.timer = null;
    this.trigger = throttle(this.trigger.bind(this), delay, true, true);
  }

  static make(reducer, callback, delay) {
    const acc = new EventAccumulator(reducer, callback, delay);
    return acc.asCallable();
  }

  static reduceTakeLast(_agg, it) {
    return it;
  }

  add(...events) {
    this.events.push(...events);
    this.trigger();
  }

  /** @private */
  trigger() {
    if (this.events.length === 0) return;

    const aggregated = this.aggregate(this.events);
    if (aggregated !== null) {
      this.callback(aggregated);
    }
    this.clear();
  }

  /** @private */
  aggregate(events) {
    if (events.length === 0) {
      return null;
    }
    return this.events.reduce((acc, e) => this.reducer(acc, e), null);
  }

  /** @private */
  clear() {
    this.events = [];
  }

  /** @private */
  get length() {
    return this.events.length;
  }

  /** @private */
  asCallable() {
    return (e) => this.add(e);
  }
}
