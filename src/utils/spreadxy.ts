import type { Dim2, Rect2 } from "./vec";


export function spreadHand(rects: Rect2[], containerRect: Dim2, { gap = 50, padding = 250, defaultWidth = 100 } = {}) {
  let totalWidth = 0;
  const widths = [];
  for (const { w = defaultWidth } of rects) {
    widths.push(w);
    totalWidth += w;
  }
  totalWidth += gap * (rects.length - 1);

  const stretch = Math.min((containerRect.w - padding * 2) / totalWidth, 1) || 1;

  const positions = [];
  let offset = 0;
  for (const w of widths) {
    offset += w / 2;

    const x = containerRect.w / 2 + (offset - totalWidth / 2) * stretch;
    const y = containerRect.h / 2;
    positions.push({ x, y });

    offset += w / 2;
    offset += gap;
  }

  return positions as Rect2[];
}
