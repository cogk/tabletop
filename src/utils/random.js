export function pseudoUlid() {
  // random + timestamp
  return Math.random().toString(36).substring(2, 9) + Date.now().toString(36);
}

export function makeId(prefix = "") {
  return prefix + pseudoUlid();
}

export function randomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

export function randomColor() {
  const constraints = {
    hue: [0, 360],
    saturation: [90, 90],
    lightness: [40, 40],
  };

  const h = randomInt(...constraints.hue);
  const s = randomInt(...constraints.saturation);
  const l = randomInt(...constraints.lightness);

  return `hsl(${h}, ${s}%, ${l}%)`;
}

export function random32() {
  return (Math.random() * 4294967296) >>> 0;
}

const _random32 = () => {
  console.warn("mulberry32 not seeded");
  return random32();
};

export function mulberry32(seed = _random32) {
  return function () {
    let t = seed += 0x6D2B79F5; // note: mutating seed
    t = Math.imul(t ^ t >>> 15, t | 1);
    t ^= t + Math.imul(t ^ t >>> 7, t | 61);
    return ((t ^ t >>> 14) >>> 0) / 4294967296;
  };
}

export const product = (a, b) => a.flatMap(x => b.map(y => [x, y]));
