export const seatingStrategies = {
  opposing: (index) => {
    const generateGroup = (index) => {
      if (index <= 1) return { offset: 0, spacing: 180, size: 2, seat: index };
      const k = Math.floor(Math.log2(index));
      const size = Math.pow(2, k);
      const offset = 180 / size;
      const spacing = 2 * offset;
      const seat = index - size; // index in the group
      return { offset, spacing, size, seat };
    };

    const group = generateGroup(index);
    return { angle: group.seat * group.spacing + group.offset };
  },
};

export function getSeatingAngle(index, count) {
  return seatingStrategies.opposing(index).angle;
}
