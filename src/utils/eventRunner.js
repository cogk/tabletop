/**
 * @typedef {object} GameEvent
 * @property {string} iid
 * @property {string} author
 * @property {string} type
 * @property {object} data
 */

import { OwnedItem } from "../items/Item";
import { hydrate } from "../net/serialize";
import { getSeatingAngle } from "../utils/seating";
import * as spreading from "../utils/spreading";

export class EventRunner {
  /**
   * @param {import("../global/app").App} app
   * @param {import("../items/GameState").GameState} state
   */
  constructor(app, state) {
    this.app = app;
    this.state = state;
  }

  async _runEvent(event) {
    let type = event.type;
    if (typeof type !== "string") return console.error("invalid event", event);
    if (!type.includes(":")) return console.error("forbidden event: too basic", event);

    if (type.includes("@")) {
      const [theType] = type.split("@");
      type = theType + "@";
    }

    /** @type {(event: GameEvent, item: any) => void} */
    const fn = this[type];

    if (fn === false) {
      return; // explicitly ignored
    }

    if (typeof fn !== "function") {
      return console.warn(`Event of type '${type}' not handled`, event);
    }

    return fn.call(this, event);
  }

  *itemsOf(event, required = false) {
    if (event.iid) {
      const item = this.app.getItem(event.iid);
      if (!item) return console.error("no item with id", event.iid, "for event (.iid)", event);
      yield item;
      required = false;
    }
    if (Array.isArray(event.iids || event.data.iids)) {
      for (const iid of event.iids || event.data.iids) {
        const item = this.app.getItem(iid);
        if (!item) return console.error("no item with id", iid, "for event (.iids)", event);
        yield item;
        required = false;
      }
    }
    if (required) {
      console.error(event);
      throw new Error("expected an item or list of items in event");
    }
  }

  ["game:item:move"](event) {
    let refreshOrder = (event.data.z !== undefined);
    for (const item of this.itemsOf(event)) {
      item.x = event.data.x;
      item.y = event.data.y;
      if ("z" in event.data) { item.z = event.data.z; }
      item.refresh();
    }
    if (refreshOrder) this.state.signalRefresh();
  }

  ["game:items:movemany"](event) {
    const items = [...this.itemsOf(event)];
    const positions = event.data.positions;
    for (let i = 0; i < items.length; i++) {
      const item = items[i];
      const pos = positions[i];
      item.x = pos?.x ?? item.x;
      item.y = pos?.y ?? item.y;
      item.z = pos?.z ?? item.z;
    }
    items.forEach((item) => item.refresh());
    this.state.signalRefresh();
  }

  ["game:item:lift"](event) {
    for (const item of this.itemsOf(event)) {
      item.z = event.data.z;
      item.refresh();
    }
    this.state.signalRefresh();
  }

  ["game:item:flipShow"](event) {
    for (const item of this.itemsOf(event)) {
      if (!(item instanceof OwnedItem)) {
        console.warn("Could not execute event on item not deriving from OwnedItem", event, item);
        continue;
      }

      item.public = true;
      item.inHand = false;
      item.refresh();
    }
  }

  ["game:item:flipHide"](event) {
    for (const item of this.itemsOf(event)) {
      if (!(item instanceof OwnedItem)) {
        console.warn("Could not execute event on item not deriving from OwnedItem", event, item);
        continue;
      }

      item.public = false;
      item.refresh();
    }
  }

  ["game:item:assignTo"](event) {
    for (const item of this.itemsOf(event)) {
      item.owners = event.data.uids.slice();
      item.refresh();
    }
    this.state.signalRefresh();
  }

  /** @param {GameEvent} event */
  ["game:item:setValues"](event) {
    const { kv } = event.data;
    for (const item of this.itemsOf(event)) {
      for (const [k, v] of Object.entries(kv)) {
        item[k] = v;
      }
      item.refresh();
    }
    this.state.signalRefresh();
  }

  ["game:item:delete"](event) {
    const { iids } = event.data;
    for (const iid of iids) {
      const item = this.app.getItem(iid);
      if (!item) continue;
      item.unmount?.();
    }
    this.state.items = this.state.items.filter((item) => !iids.includes(item.iid));

    const selection = this.app.selection.selectedElements
      .filter((iid) => !iids.includes(iid));
    this.app.selection.selectElements(selection);
    this.state.signalRefresh();
  }

  ["game:items:spread"](event) {
    spreading.applySpreadOnIds(this.app, event.data.iids, spreading[event.data.method], event);
  }

  ["game:spawn"](event) {
    try {
      const serializedItems = event.data.items ?? [event.data.item];
      const items = hydrate(serializedItems);
      this.state.items.push(...items);
      for (const item of items) {
        item.mountInApp(this.state.app);
      }
      this.state.signalRefresh();
    } catch (e) {
      console.error(e);
      debugger; // eslint-disable-line no-debugger
      throw e;
    }
  }

  ["net:intro:welcome"](event) {
    this.state.takeFrom(event.data.state);
    // const myIndex = this.state.players.length - 1
    const myIndex = this.state.players.findIndex((p) => p.uid === this.app.player.uid);
    const angle = getSeatingAngle(myIndex);
    this.app.panner.setRotation(angle);
  }

  ["net:joined"](event) {
    const player = { ...event.data };
    const uid = player?.uid;
    if (!uid) throw new Error("Player that is joining the game has no uid");

    const s = this.state;
    if (s.players.find((p) => p.uid === uid)) {
      s.players = s.players.filter((p) => p.uid !== uid);
    }
    s.players.push(player);
    s.signalRefresh();
  }

  ["game:cursor:move"] = false;
  ["game:cursor:clear"] = false;
  ["game:hover:item@"] = false;
  ["game:hover:zone@"] = false;
}
