import { Bus } from "./utils/bus";

export const uiBus = new Bus("ui");
export const gameBus = new Bus("main");
