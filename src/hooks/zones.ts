import { Ref, isRef, onMounted, onUnmounted } from "vue";
import { Delta2, Rect2, Vec2, isInBounds, vecDiff } from "../utils/vec";

export function useZone<T extends Zone>(zone: T): T {
  onMounted(() => {
    ZoneManager.instance.zones.set(zone.zid, zone);
  });
  onUnmounted(() => {
    ZoneManager.instance.zones.delete(zone.zid);
  });
  return zone;
}

class Zone {
  zid: string;

  constructor(zid: string) {
    this.zid = zid;
  }

  convertDeltaToWorldDelta(delta: Delta2): Delta2 { // eslint-disable-line @typescript-eslint/no-unused-vars
    throw new Error("Not implemented");
  }

  hitTestScreen(screen: Vec2): boolean { // eslint-disable-line @typescript-eslint/no-unused-vars
    throw new Error("Not implemented");
  }
}

export class ScreenZone extends Zone { // Absolutely positioned zone on the screen (e.g. hand)
  _rect: Rect2 | Ref<Rect2>;

  constructor(zid: string, rect: Rect2 | Ref<Rect2>) {
    if (!rect) throw new Error("ScreenZone: rect is required");
    super(zid);
    this._rect = rect;
  }

  get rect() {
    return isRef(this._rect) ? (this._rect as Ref<Rect2>).value : (this._rect as Rect2);
  }

  hitTestScreen(screen: Vec2) {
    return isInBounds(screen, this.rect);
  }

  convertToLocal(screen: Vec2) {
    return vecDiff(screen, this.rect);
  }

  convertDeltaToLocalDelta({ dx, dy }: Delta2): Delta2 {
    // No conversion needed: screen delta is already local delta (no rotation, 1:1 ratio)
    return { dx, dy };
  }
}

export class ZoneManager {
  static _instance: ZoneManager;
  static get instance() {
    if (!this._instance) {
      this._instance = new ZoneManager();
    }
    return this._instance;
  }

  static GetZidForElement(element: HTMLElement) {
    const zid = element.closest("[data-zid]")?.getAttribute("data-zid");
    return zid || undefined;
  }

  zones: Map<string, Zone> = new Map();

  getZoneByZid(zid: string) {
    const zone = this.zones.get(zid);
    if (!zone) {
      throw new Error(`Zone '${zid}' not found`);
    }
    return zone;
  }

  getZoneForElement(element: HTMLElement) {
    const zid = ZoneManager.GetZidForElement(element);
    if (zid) {
      return this.getZoneByZid(zid);
    }
  }
}
