import { ref, shallowRef } from "vue";
import { watchEventBus } from "./index";

export function useHoverState(xid: string, xtype: "item" | "zone" = "item") {
  const hoverCount = shallowRef(0);
  const hoveredByMe = shallowRef(false);
  const hoverers = ref(new Set<string>());

  watchEventBus(`game:hover:${xtype}@${xid}`, (event) => {
    const uid = event.uid;

    if (event.data.state) {
      hoverCount.value += 1;
      hoverers.value.add(uid);
    } else {
      hoverCount.value -= 1;
      hoverers.value.delete(uid);
    }

    hoveredByMe.value = hoverers.value.has(uid);
  });

  return { hoverCount, hoveredByMe, hoverers };
}
