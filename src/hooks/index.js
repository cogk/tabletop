import { computed, inject, isRef, onMounted, onUnmounted, ref, shallowRef } from "vue";
import { gameBus, uiBus } from "../bus";

/**
 * @function
 * @template T
 * @param {T} fn
 * @returns {T}
 **/
export function asSingleton(fn) {
  const cache = new Map();
  return (...args) => {
    const key = JSON.stringify(args);
    if (!cache.has(key)) {
      // console.log(`%c${fn.name}(${key})`, "color: #555;");
      cache.set(key, fn(...args));
    }
    return cache.get(key);
  };
}

const rnd = Math.round;

export function useTransform(x, y) {
  if (x && typeof x === "object" && y === undefined) ({ x, y } = x);
  return { transform: _translate(x, y), x, y };
}

function _translate(x, y, unit = "") {
  return `translate(${rnd(x)}${unit}, ${rnd(y)}${unit})`;
}

export function translate({ x, y }, unit = "") {
  return _translate(x, y, unit);
}

/**
 * @param {string} iid
 * @returns {import('vue').Ref<import('../items/Item').Item>}
 */
export function useItem(item) {
  const itemId = item?.iid ?? item;

  if (typeof itemId !== "string") throw new Error("useItem: invalid item id");

  const itemRef = shallowRef(item);

  watchUiBus("ui:refresh:item@" + itemId, (event) => {
    itemRef.value = null;
    itemRef.value = event.data?.item ?? item;
  });

  return itemRef;
}

/**
 * @param {string} iid
 * @param {(item: (import('../items/Item').Item) => void)} callback
 */
export function watchItem(iid, callback) {
  const app = useApp();
  const item = app.getItem(iid);

  watchUiBus("ui:refresh:item@" + iid, () => {
    callback(item);
  });

  onMounted(() => {
    callback(item);
  });
}

export function watchSignal(signal, callback) {
  let off;
  onMounted(() => {
    off = signal.watch(callback);
  });
  onUnmounted(() => {
    off?.();
    off = null;
  });
}

/**
 * @returns {import('vue').Ref<import('../items/GameState').GameState>}
 */
export function useGameState() {
  const app = useApp();
  const state = shallowRef(app.state);
  const upd = () => {
    state.value = null;
    state.value = app.state;
  };
  // watchUiBus(/ui:refresh:item@/, upd);
  watchSignal(app.state.signalRefresh, upd);
  return state;
}

/**
 * @param {string} iid
 */
export function useSelected(iid) {
  const selection = useSelectedItems();
  return computed(() => selection.value.includes(iid));
}

export function useSelectedItems() {
  const initialValue = useApp().selection?.selectedElements.slice() ?? [];
  const selection = shallowRef(initialValue);
  watchUiBus("ui:select", (event) => {
    const selected = event.data?.selected ?? [];
    selection.value = [...selected];
  });
  return selection;
}

// export function useSize({ w, h }) {
//   return { width: w, height: h };
// }

export function usePositionTL(x, y) {
  if (x && typeof x === "object" && y === undefined) ({ x, y } = x);
  const translate = `translate(${rnd(x)}px, ${rnd(y)}px)`;
  return `position: fixed; top: 0px; left: 0px; transform: ${translate};`;
  // return `position: fixed; top: ${CSS.px(rnd(y))}; left: ${CSS.px(rnd(x))};`;
}

/**
 * @param {import("vue").Ref<import("../items/Item").Item>} itemRef
 */
export function useItemProps(itemRef) {
  const selected = useSelected(itemRef.value.iid);

  const rootProps = computed(() => {
    const controlled = inject("controlled", null);

    const item = itemRef.value;
    const { x, y } = controlled?.position?.value ?? item;
    const { w = 0, h = 0 } = item;
    return {
      "data-iid": item.iid,
      "style": {
        "--item-x": CSS.px(x - w/2),
        "--item-y": CSS.px(y - h/2),
      },
      "data-x": x,
      "data-y": y,
      "class": [
        "interactable",
        "i--move",
        "i--hoverable",
        selected.value && "i--selected",
      ],
    };
  });

  return rootProps;
}

export function useOwnedItemProps(itemRef) {
  const ownedByMe = computed(() => itemRef.value.owners.includes(useApp().player.uid));
  const isForbidden = computed(() => itemRef.value.owners.length && !ownedByMe.value && !itemRef.value.public);

  // const couldBeVisibleToAnyone = computed(() => !itemRef.value.owners.length || itemRef.value.public);
  // const couldBeVisibleToMe = computed(() => !itemRef.value.owners.length || ownedByMe.value && itemRef.value.public);

  const inMyHandPrivate = computed(() => itemRef.value.inHand && !itemRef.value.public && ownedByMe.value);

  // const handProps = computed(() => itemRef.value.inHand ? { "transform": "rotate(10) scale(0.7)" } : null);
  const handProps = computed(() => itemRef.value.inHand ? { "data-in-hand": true } : null);

  return {
    ownedByMe,
    inMyHandPrivate,
    isForbidden,
    handProps,
  };
}

export function useMergedObjects(...objects) {
  const merge = (a, b, key) => {
    if (isRef(a)) { a = a.value; }
    if (a == null) return b; // eslint-disable-line eqeqeq

    if (isRef(b)) { b = b.value; }
    if (b == null) return a; // eslint-disable-line eqeqeq

    if (Array.isArray(a) && Array.isArray(b)) return [...a, ...b];

    if (key === "transform" && typeof a === "string" && typeof b === "string") {
      return a + " " + b;
    }

    if (typeof a !== "object" || typeof b !== "object") return b;

    const out = {};
    const keys = new Set([...Object.keys(a), ...Object.keys(b)]);
    for (const key of keys) {
      out[key] = merge(a[key], b[key], key);
    }
    return out;
  };
  return computed(() => objects.reduce(merge, undefined));
}

function _watchBus(bus, eventName, callback) {
  let off;
  onMounted(() => {
    if (eventName instanceof RegExp) {
      off = bus.onRegex(eventName, callback);
    } else {
      off = bus.on(eventName, callback);
    }
  });
  onUnmounted(() => {
    off?.();
    off = null;
  });
}

export function watchEventBus(eventName, callback) {
  return _watchBus(gameBus, eventName, callback);
}

export function watchUiBus(eventName, callback) {
  return _watchBus(uiBus, eventName, callback);
}

/** @returns {import("../global/app").App} */
export function useApp() {
  return inject("app");
}

/** @returns {import('../items/GameState').GameState} */
export function useRawState() {
  return inject("state");
}

function _useCamera() {
  const app = useApp();

  const getCamera = () => {
    if (!app.panner) return null;
    const center = app.panner.getWorldPos();
    const size = app.panner.getViewportSize();
    const { zoomLevel, rotationAngle } = app.panner;
    return { center, viewportSize: size, size, zoomLevel, rotationAngle };
  };

  const camera = shallowRef(getCamera());

  let off;
  onMounted(() => {
    off = uiBus.on("ui:camera", () => {
      camera.value = getCamera();
    });
  });
  onUnmounted(() => {
    off?.();
    off = null;
  });

  return camera;
}

export const useCamera = asSingleton(_useCamera);


/** @returns {import("vue").Ref<"local"|"host"|"client">} */
export function useP2PState() {
  const state = ref("local");
  watchEventBus("net:p2p:now-hosting", () => state.value = "host");
  watchEventBus("net:p2p:now-hosted", () => state.value = "client");
  watchEventBus("net:p2p:now-local", () => state.value = "local");
  return state;
}

const _useUpright = asSingleton(function _useUpright() {
  const camera = useCamera();
  return computed(() => ({
    "transform": `rotate(${-camera.value.rotationAngle}deg)`,
    "transform-origin": "center center",
    "transform-box": "fill-box",
  }));
});
export const useUpright = () => {
  const upright = _useUpright();
  return computed(() => {
    if (inject("controlled", null)) {
      return {};
    } else {
      return upright.value;
    }
  });
};

export function useHand(uid) {
  const app = useApp();
  const hand = {
    /** @type {Ref<(import("../items/Item").Item)[]>} */
    items: shallowRef([]),
    owner: uid,
  };

  const ownedByMe = (item) => item.owners?.[0] === uid;

  const refresh = () => {
    const newHand = app.state.items.filter((item) => item.inHand && ownedByMe(item));
    hand.items.value = newHand;
  };

  app.state.signalRefresh.watch(refresh);
  watchUiBus(/ui:item:refresh:/, refresh);

  return hand;
}
