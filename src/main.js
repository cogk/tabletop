import { createApp } from "vue";
import appBody from "./view/app-body.vue";

if (window.matchMedia("(pointer: coarse)").matches) {
  window.onerror = function (message, source, lineno, colno, error) {
    console.log("Error: ", message, source, lineno, colno, error);
    alert("Error: " + message);
  };
}

const vue = createApp(appBody);
vue.mount("#app-body");
