const fr = {
  "Token": "Jeton",
  "PokerCard": "Carte (poker)",
};

export const __ = (key) => fr[key] || key;
