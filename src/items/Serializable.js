const identity = (x) => x;

function pick(obj, keys, transform = identity) {
  const out = {};
  for (const key of keys) {
    if (obj[key] === undefined) continue;
    out[key] = transform(obj[key]);
  }
  return out;
}

class BaseSerializable {
  static type = "_INTERNAL_baseserializable";
  getType() { return this.constructor.type; }

  /**
   * @param {object} dep
   * @param {Record<string, any>} dep.serialized is the serialized input object
   * @param {(dep: object) => Serializable} dep.hydrator is a function that can be used to hydrate sub-items.
   * @returns {Record<string, any>}
   */
  hydrate({ serialized }) {
    if (serialized.type !== this.getType()) {
      throw new Error("Expected types to match");
    }
    return this;
  }

  /**
   * @param {object} _dep
   * @param {(dep: object) => Record<string, any>} _dep.serializer is the serializer function
   * @returns {const Record<string, any>}
   */
  serialize(_dep) {
    return { type: this.getType() }; // eventually freezed
  }

  giving() { throw new Error("Not implemented"); }
  taking() { throw new Error("Not implemented"); }
}

export class Serializable extends BaseSerializable {
  static type = "_serializable";

  /**
   * @param {string[]} keys
   * @param {object} dep
   * @param {Record<string, any>} dep.serialized is the serialized input object
   * @param {(dep: object) => Serializable} dep.hydrator is a function that can be used to hydrate sub-items.
   * @param {Function} next
   * @returns {Record<string, any>}
   */
  taking(keys, dep, next) {
    const { serialized, hydrator } = dep;
    if (typeof hydrator !== "function") throw new Error("Expected hydrator to be a function");
    const taken = pick(serialized, keys, hydrator);
    Object.assign(this, taken, next.call(this, dep));
    return this;
  }

  /**
   * @param {string[]} keys
   * @param {object} dep
   * @param {(dep: object) => Record<string, any>} dep.serializer is the serializer function
   * @param {Function} next
   * @returns {Record<string, any>}
   */
  giving(keys, dep, next) {
    const { serializer } = dep;
    if (typeof serializer !== "function") throw new Error("Expected serializer to be a function");
    const given = pick(this, keys, serializer);
    Object.assign(given, next.call(this, dep));
    return given;
  }

  toString() {
    return `<${this.getType()}>`;
    // const serializer = (x) => {
    //   if (x instanceof Serializable) {
    //     return x.serialize({ serializer });
    //   }
    //   return x;
    // };
    // return `<${this.getType()}>:${JSON.stringify(this.serialize({ serializer }))}`;
  }

  /** @private */
  mount() { throw new Error("Cannot mount a generic serializable."); }
}
