import { Serializable } from "./Serializable";

export class TSet extends Serializable {
  static type = "set";
  static keys = ["values"];

  values = new Set();

  /** @type {typeof Serializable.prototype.hydrate} */
  hydrate(X) {
    super.hydrate(X);
    this.values = new Set(X.serialized.values);
    return this;
  }

  /** @type {typeof Serializable.prototype.serialize} */
  serialize(X) {
    const out = super.serialize(X);
    out.values = X.serializer([...this.values]);
    return out;
  }

  toString() {
    const list = [...this.values.values()]
      .sort()
      .map((x) => JSON.stringify(x));
    return `TSet{${list.join(", ")}}`;
  }

  /** @type {(value: T) => TSet} */
  add(value) {
    this.values.add(value);
    return this;
  }

  /** @type {() => TSet} */
  clear() {
    this.values.clear();
    return this;
  }

  /** @type {(value: T) => TSet} */
  delete(value) {
    this.values.delete(value);
    return this;
  }

  /** @type {(value: T) => boolean} */
  has(value) {
    return this.values.has(value);
  }

  /** @type {(callback: (value: T) => void) => void} */
  forEach(callback) {
    return this.values.forEach(callback);
  }

  get size() {
    return this.values.size;
  }
}
