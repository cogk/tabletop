import { uiBus, gameBus } from "../bus";
import { makeId } from "../utils/random";
import { Serializable } from "./Serializable";

export class Item extends Serializable {
  static type = "_genericitem";
  static keys = ["iid", "x", "y", "z"];

  /** @type {import("../global/app").App} */
  #app = null;

  iid = makeId(this.getType() + ":");
  x = 0;
  y = 0;
  z = Date.now();

  hydrate(X) { return this.taking(Item.keys, X, super.hydrate); }
  serialize(X) { return this.giving(Item.keys, X, super.serialize); }

  /** @param {import("../global/app").App} app */
  mountInApp(app) {
    this.#app = app;
    const bus = app.makeLazyBusForItem(this.iid);
    return this.mount({ bus, app, uiBus, gameBus });
  }

  /**
   * @param {object} _opts
   * @param {import("../global/app").App} _opts.app
   * @param {import("../utils/bus").Bus } _opts.bus
   * @param {import("../utils/bus").Bus } _opts.gameBus
   * @param {import("../utils/bus").Bus } _opts.uiBus
   */
  mount(_opts) {}

  get isMounted() {
    return Boolean(this.#app);
  }

  refresh() {
    if (!this.isMounted) return console.warn("Item not mounted, cannot refresh");
    uiBus.emitEvent({
      iid: this.iid,
      type: "ui:refresh:item@" + this.iid,
      data: { item: this },
    });
  }
}

export class OwnedItem extends Item {
  static type = "_owneditem";
  static keys = ["owners", "public", "inHand"];

  owners = [];
  public = false;
  inHand = false;

  hydrate(X) { return this.taking(OwnedItem.keys, X, super.hydrate); }
  serialize(X) { return this.giving(OwnedItem.keys, X, super.serialize); }
}


// eslint-disable-next-line no-unused-vars
class TemplateItem extends Item {
  static type = "myAmazingItem"; // a unique value used for serialization and iid generation
  static keys = ["field_1", "field_2"]; // a list of keys that should be serialized

  // Never add a custom constructor(), especially not one that takes arguments.
  // Instead, use the `hydrate` method to initialize your item,
  // and declare your fields directly in the class definition as shown below:
  field_1 = 42; // a default value for field_1
  field_2 = "hello world"; // a default value for field_2

  // Boilerplate code for serializing/deserializing the item.
  hydrate(X) { return this.taking(TemplateItem.keys, X, super.hydrate); }
  serialize(X) { return this.giving(TemplateItem.keys, X, super.serialize); }

  /** @type {typeof Item.prototype.mount} */
  mount({ app, bus, gameBus }) {
    // The `mount` method is the heart of the item, and is where you should register event listeners, etc.
    // This method is called when the item is mounted, just after it is created/deserialized.

    // `app` is the global app object, and can be used to access the game state to query the current player, etc.
    // `bus` is the local event bus, and can be used to listen for events specific to this item.
    // `gameBus` is the global event bus, transmitting all events (new item, player joined, button clicked, etc.).

    console.log("Players are:", app.state.players);

    bus.on("game:item:move", ({ data: { x, y } }) => {
      console.log(`I was moved to (${x}, ${y})`);
    });

    gameBus.on("game:item:move", ({ data: { iid, x, y }, author }) => {
      console.log(`Item '${iid}' moved to (${x}, ${y}) by user with id '${author}'`);
    });

    gameBus.on("net:joined", ({ data: { displayName, uid } }) => {
      console.log(`Player '${displayName}' (id: ${uid}) just joined the game.`);
    });
  }
}
