import { PhClub } from "@phosphor-icons/vue";
import { __ } from "../content/messages";
import { mulberry32, random32 } from "../utils/random";
import { OwnedItem } from "./Item";
import pokerCardVue from "./poker-card.vue";
import itemInvalidIndicatorUrl from "/invalid.png";

// const ALL_VALUES = Array.from({ length: 13 }, (_, i) => i + 1);
const ALL_SUITS = ["spades", "hearts", "clubs", "diamonds", "joker"];

const makeStandardDeckTemplate = () => {
  const out = [];
  const rng = mulberry32(random32());
  const r = () => {
    const d = 25 * Math.sqrt(rng());
    const a = 360 * rng();
    let x = d * Math.cos(a);
    let y = d * Math.sin(a);
    return { x, y };
  };
  for (const suit of ALL_SUITS.slice(0, 4)) {
    for (let value = 1; value <= 13; value++) {
      out.push({ value, suit, ...r() });
    }
  }
  return out;
};

export class PokerCard extends OwnedItem {
  static type = "pokercard";
  static keys = ["value", "suit"];
  static displayName = __("PokerCard");
  static templates = [
    { items: [{}] },
    {
      displayName: "Paquet de 52 cartes",
      icon: PhClub,
      items: makeStandardDeckTemplate,
    },
  ];

  value = 1;
  suit = "joker";

  hydrate(X) { return this.taking(PokerCard.keys, X, super.hydrate); }
  serialize(X) { return this.giving(PokerCard.keys, X, super.serialize); }

  render() {
    return pokerCardVue;
  }

  static lutUnicodeSuit = { "spades": "♠", "hearts": "♥", "clubs": "♣", "diamonds": "♦", "joker": "🃏" };
  static lutSuit = { "spades": "S", "hearts": "H", "clubs": "C", "diamonds": "D", "joker": "joker" };
  static lutValue = { 11: "J", 12: "Q", 13: "K" };

  get key() {
    return `${PokerCard.lutSuit[this.suit] ?? this.suit}${PokerCard.lutValue[this.value] ?? this.value}`;
  }
  toString() {
    return `Card#${this.id}(${PokerCard.lutUnicodeSuit[this.suit] ?? this.suit}${PokerCard.lutValue[this.value] ?? this.value})`;
  }
  get unicodeName() {
    return `${PokerCard.lutUnicodeSuit[this.suit] ?? this.suit}${PokerCard.lutValue[this.value] ?? this.value}`;
  }

  get w() { return 125; }
  get h() { return 1.4 * 125; }

  get sortOrder() {
    return ALL_SUITS.indexOf(this.suit) * 13 + (this.value - 1);
  }

  static getCardRenderingInfo(/** @type {PokerCard} */ item, /** @type {import("../global/app").App} */ app) {
    return {
      get attrs() {
        return {
          // "image-rendering": "pixelated"
        };
      },

      get teamColor() {
        if (!item.owners.length) return null;
        const owner = app.state.getPlayer(item.owners[0]);
        return owner?.teamColor ?? owner?.color ?? null;
      },

      get frontFaceUrl() {
        return cardStyles.current.front(item);
      },

      get backFaceUrl() {
        return cardStyles.current.back();
      },
    };
  }

  static prefetch(fetcher) {
    // request all card images to be cached
    for (const url of Object.values(cardStyles.current.cards)) {
      fetcher(url);
    }
  }
}

const cardStyles = {
  get current() {
    return cardStyles.classic;
  },
  classic: {
    cards: import.meta.glob("/node_modules/standard-deck/dist/standard-deck/*.png", { as: "url", eager: true }),
    front(item) {
      const url = `/node_modules/standard-deck/dist/standard-deck/front-${item.sortOrder}.png`;
      return this.cards[url] ?? itemInvalidIndicatorUrl;
    },
    back() {
      return this.cards["/node_modules/standard-deck/dist/standard-deck/back.png"];
    },
  },
  // pixel: {
  //   cards: import.meta.glob("/assets/greebles.itch.io-pixel-plebes/*.png", { as: "url", eager: true }),
  //   front(item) {
  //     const suitName = PokerCard.lutSuit[item.suit] ?? item.suit;
  //     const valueName = PokerCard.lutValue[item.value] ?? item.value;
  //     const fileName = `${suitName}${valueName}`;
  //     const url = `/assets/greebles.itch.io-pixel-plebes/${fileName}.png`;
  //     return this.cards[url] ?? itemInvalidIndicatorUrl;
  //   },
  //   back() {
  //     const url = "/assets/greebles.itch.io-pixel-plebes/back.png";
  //     return this.cards[url];
  //   },
  // },
};
