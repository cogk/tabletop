import { GameState as gamestate } from "./GameState";
import { PokerCard as pokercard } from "./PokerCard";
import { ItemToken as token } from "./Token";

export const knownTypes = { token, gamestate, pokercard };
