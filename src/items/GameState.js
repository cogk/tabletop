import { gameBus } from "../bus";
import { hydrate } from "../net/serialize";
import { EventFactory } from "../utils/eventFactory";
import { EventRunner } from "../utils/eventRunner";
import { Signal } from "../utils/signal";
import { EventGlue } from "../view/glue";
import { Serializable } from "./Serializable";
import { TSet } from "./TSet";

export class GameState extends Serializable {
  static type = "gamestate";
  static keys = ["items", "players"];

  /** @type {(import("./Item").Item)[]} */
  items = [];
  players = [];
  features = new TSet();

  /** @type {import("../global/app").App} */
  app = null;
  signalRefresh = new Signal();

  hydrate(X) { return this.taking(GameState.keys, X, super.hydrate); }
  serialize(X) { return this.giving(GameState.keys, X, super.serialize); }

  takeFrom(serialized) {
    this.hydrate({ serialized, hydrator: hydrate });
    for (const item of this.items) {
      if (!item.isMounted) {
        item.mountInApp(this.app);
      }
    }
    this.signalRefresh();
  }

  mountInApp(app) {
    this.app = app;
    this.eventGlue = new EventGlue(app, this);
    this.eventRunner = new EventRunner(app, this);
    this.eventFactory = new EventFactory(app, this);

    gameBus.onRegex(/^(game:.*|net:intro:welcome|net:joined)$/, (event) => {
      this.eventRunner._runEvent(event);
    });
  }

  _getChildItem(group, iid) {
    const item = this[group].find((item) => item.iid === iid);
    if (!item) {
      console.warn(`No item with iid '${iid}' found in ${this.getType()}.${group}`);
      return null;
    }
    return item;
  }

  addPlayer(player) {
    if (player.uid === this.app.player.uid) {
      this.app.player = player;
    }
    this.players = this.players.filter((p) => p.uid !== player.uid);
    this.players.push(player);
    this.signalRefresh();
  }

  getPlayer(uid) {
    return this.players.find((p) => p.uid === uid);
  }
}
