import { __ } from "../content/messages";
import { randomColor } from "../utils/random";
import { Item } from "./Item";
import tokenItemVue from "./token-item.vue";

export class ItemToken extends Item {
  static type = "token";
  static keys = ["color"];
  static displayName = __("Token");
  static templates = true;

  color = randomColor();

  hydrate(X) { return this.taking(ItemToken.keys, X, super.hydrate); }
  serialize(X) { return this.giving(ItemToken.keys, X, super.serialize); }

  get w() { return 48; }
  get h() { return 48; }

  render() { return tokenItemVue; }
}
