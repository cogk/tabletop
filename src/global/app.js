import { gameBus, uiBus } from "../bus";
import { Bus } from "../utils/bus";
import { Auth } from "../net/auth";
import { P2P } from "../net/p2p";
import { hydrate, hydroserialize } from "../net/serialize";
import { makeId, randomColor } from "../utils/random";
import { BtnHost } from "../view/btn/host";
import { BtnJoin } from "../view/btn/join";
import { DebugTooltip } from "../view/debug-tooltip";
import { EffectsManager } from "../view/effects";
import { MouseSelect } from "../view/mouseSelect";
import { Move } from "../view/move";
import { Panner } from "../view/panner.interactjs";
import { knownTypes } from "../items";
import { HoverHandler } from "../view/hover";

const roomId = "vskyrlv2qqh7q6eobnegyqok";

export class App {
  constructor() {
    const fromLocalStorage = {
      displayName: localStorage.getItem("tabletop:displayName") || makeId("anon_"),
      uid: localStorage.getItem("tabletop:uid") || makeId("user_"),
    };

    this.player = {
      uid: fromLocalStorage.uid,
      displayName: fromLocalStorage.displayName,
      color: randomColor(),
    };

    /** @type {import("../items/GameState").GameState} */
    this.state = hydrate({ type: "gamestate" });

    this.p2p = null;
    this.p2p_host = null;

    window.app = this;
    this.gameBus = gameBus;
    this.uiBus = uiBus;

    this.initBusses();
  }

  initBusses() {
    this.effects = new EffectsManager(this);
    this.busses = {};
    gameBus.onRegex(/game:items?:/, (event) => {
      if (event.iid) {
        this.busses[event.iid]?.emitEvent(event);
      }
      if (Array.isArray(event.iids || event.data?.iids)) {
        for (const iid of (event.iids || event.data.iids)) {
          this.busses[iid]?.emitEvent(event);
        }
      }
    });
  }

  initListeners() {
    /** @type {SVGSVGElement} */
    const tabletopElement = document.getElementById("tabletop");
    const selectionElement = document.getElementById("tabletop-selection-box");

    this.panner = new Panner();
    this.panner.start(tabletopElement);

    this.selection = new MouseSelect();
    this.selection.start(tabletopElement, selectionElement);

    this.movement = new Move(this);
    this.debugTooltip = new DebugTooltip();
    this.hoverHandler = new HoverHandler(this);
  }

  start() {
    this.initListeners();

    this.state.mountInApp(this);
    this.state.addPlayer(this.player);

    this.panner.update();

    new BtnHost(document.getElementById("app-btn-host"), this);
    new BtnJoin(document.getElementById("app-btn-join"), this);

    this.prefetchAssets();
  }

  p2pStop() {
    this.p2p?.destroy();
    this.p2p = null;
    this.p2p_host?.destroy();
    this.p2p_host = null;
  }

  async p2pHost() {
    this.p2pStop();
    const { client, host } = await P2P.becomeHost(roomId, this);
    this.p2p = client;
    this.p2p_host = host;
    gameBus.emitEvent({ type: "net:p2p:now-hosting" });
  }

  async p2pJoin() {
    this.p2pStop();
    this.p2p = await P2P.becomeClient(roomId, this);
    gameBus.emitEvent({ type: "net:p2p:now-hosted" });
  }

  /** @returns {import("../utils/bus").Bus} */
  makeLazyBusForItem(iid) {
    return new Proxy({}, { get: (_target, prop) => this._getOrMakeBusForItem(iid)[prop] });
  }

  /** @param {string} iid */
  _getOrMakeBusForItem(iid) {
    if (!iid) throw new Error("iid is required");
    let bus = this.busses[iid];
    if (!(bus instanceof Bus)) {
      this.busses[iid] = bus = new Bus();
    }
    return bus;
  }

  emitEventIfAllowed(event) {
    const msg = Auth.check(this.state, event);
    if (msg) {
      console.error(msg);
      return uiBus.emitEvent({
        type: "ui:notify:forbidden",
        data: { message: msg, forbiddenEvent: event },
      });
    }
    gameBus.emitEvent(event);
  }

  /**
   * @param {string} iids
   * @returns {import("../items/Item").Item}
   */
  getItem(iid) {
    return this.state?._getChildItem("items", iid);
  }

  /**
   * @param {string[]} iids
   * @returns {import("../items/Item").Item[]}
   */
  getItems(iids) {
    return iids.map((iid) => this.getItem(iid)).filter((item) => item);
  }

  spawnItems(...items) {
    this.emitEventIfAllowed({
      type: "game:spawn",
      data: { items: hydroserialize(items) },
    });
  }

  prefetchAssets() {
    const fetcher = (url) => {
      fetch(url).catch(err => console.warn(`Failed to prefetch ${url}`, err));

      // NOTE: spams the console with warnings because the assets are not used after being prefetched
      // const link = document.createElement("link");
      // Object.assign(link, { rel: "preload", href: url, as: "image" });
      // document.head.appendChild(link);
    };
    for (const type of Object.values(knownTypes)) {
      type.prefetch?.(fetcher);
    }
  }
}

export const app = new App();
