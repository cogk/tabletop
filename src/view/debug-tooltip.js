export class DebugTooltip {
  constructor() {
    this.outputElement = document.createElement("div");
    this.outputElement.id = "debug-tooltip";

    Object.assign(this.outputElement.style, {
      position: "fixed",
      bottom: "0",
      left: "0",
      zIndex: "1000",
      border: "1px solid #f0f",
      fontFamily: "monospace",
      background: "#fff",
      color: "#000",
      padding: "0.25em 0.5em",
      visibility: "hidden",
    });

    document.body.append(this.outputElement);

    document.body.addEventListener("mouseover", this.onMouseEnter.bind(this));
    document.body.addEventListener("mouseout", this.onMouseLeave.bind(this));
  }

  onMouseEnter(event) {
    // Check if element is inside a `svg`, and has a `title` attribute
    const title = event.target.closest("svg [title]")?.getAttribute("title");
    if (title) {
      this.outputElement.innerText = title;
      this.outputElement.style.visibility = "visible";
    }
  }

  onMouseLeave(_event) {
    this.outputElement.style.visibility = "hidden";
  }
}
