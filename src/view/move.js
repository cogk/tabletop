// https://interactjs.io/docs/tooling/#feature-selection
import interact from "interactjs";
import { uiBus } from "../bus";
import { getElementItemXY, getItemIdFromElement } from "../utils/dom";
import { KeyboardHandler } from "./keyboard";
import { ZoneManager } from "../hooks/zones";

export class Move {
  /** @param {import("../global/app").App} app */
  constructor(app) {
    this.app = app;
    this.listenAll();
  }

  listenAll() {
    interact(".i--move", {
      drag: {
        origin: document.body,
        // inertia: { enabled: true, resistance: 40 },
        onstart: (event) => {
          if (event.interaction?.tabletop === undefined) {
            event.interaction.tabletop = {
              // Keep track of the selected elements at the beginning of the drag
              selectedIds: this.app.selection?.selectedElements,
            };
          }
        },
        onend: (event) => {
          event.interaction.tabletop = undefined;
        },
        onmove: (event) => {
          /** @type {Set<SVGElement>} */
          let elements = new Set();
          elements.add(event.target);

          for (const id of event.interaction?.tabletop?.selectedIds ?? []) {
            const el = document.querySelector(`[data-iid="${id}"]`);
            if (el) elements.add(el);
          }
          elements = [...elements.values()];

          const _deltas = {}; // zone -> delta
          const getDelta = (zid) => {
            const zone = Boolean(zid) && ZoneManager.instance.getZoneByZid(zid);
            if (!zone) {
              // Board
              return this.app.panner.convertScreenDeltaToWorldDelta(event.dx, event.dy);
            } else {
              // Zone
              if (_deltas[zone.zid] === undefined) {
                const delta = zone.convertDeltaToLocalDelta(event);
                _deltas[zone.zid] = delta;
              }
              return _deltas[zone.zid];
            }
          };

          const getInfo = (element) => {
            const iid = getItemIdFromElement(element);
            const from = getElementItemXY(element);
            const zid = ZoneManager.GetZidForElement(element);
            const delta = getDelta(zid);
            const to = { x: from.x + delta.dx, y: from.y + delta.dy };
            return { iid, from, to, zid };
          };

          if (elements.length === 0) {
            return;
          } else if (elements.length === 1) {
            const element = elements[0];
            const { iid, from, to, delta, zid } = getInfo(element);
            uiBus.emit("ui:interact:move", { iid, zid, element, from, to, delta, _e: event });
          } else {
            /** @type {Map<string | undefined, unknown[]>} */
            const byZone = new Map();
            for (const element of elements) {
              const info = getInfo(element);
              const zid = info.zid ?? "@@BOARD";
              if (!byZone.has(zid)) byZone.set(zid, []);
              byZone.get(zid).push(info);
            }
            for (const [zid, infos] of byZone.entries()) {
              const delta = infos[0].delta;
              const iids = [];
              const positions = [];
              for (const { iid, to } of infos) {
                iids.push(iid);
                positions.push(to);
              }
              uiBus.emit("ui:interact:movemany", { iids, positions, zid, delta, _e: event });
            }
          }
        },
        // modifiers: this._getModifiers(),
      },
    });

    interact(".interactable").on("down", (event) => {
      if (event.originalEvent.button !== 0) return;
      const element = event.currentTarget;
      uiBus.emit("ui:interact:down", { element, _e: event });
    }).on("up", (event) => {
      if (event.originalEvent.button !== 0) return;
      const element = event.currentTarget;
      uiBus.emit("ui:interact:up", { element, _e: event });
    }).on("doubletap", (event) => {
      const element = event.currentTarget;
      uiBus.emit("ui:interact:doubletap", { element, _e: event });
    });

    // interact(".interactable--forbidden").on("down", (event) => {
    //   const element = event.currentTarget;
    //   const iid = element.getAttribute("iid");
    //   return uiBus.emitEvent({
    //     type: "ui:notify:forbidden",
    //     data: { message: "forbidden", forbiddenEvent: { iid } },
    //   });
    // });
  }

  _getModifiers() {
    const largeGrid = interact.snappers.grid({
      x: 125 / 4,
      y: 125 / 4,
      range: Infinity,
      offset: { x: 0, y: 0 },
    });

    return [
      interact.modifiers.snap({
        targets: [
          interact.snappers.grid({
            x: 1,
            y: 1,
            range: Infinity,
            offset: { x: 0, y: 0 },
          }),
        ],
        range: Infinity,
        relativePoints: [{ x: 0, y: 0 }],
      }),

      // Snap to grid if shift is pressed
      interact.modifiers.snap({
        targets: [
          (x, y, interaction, offset, index) => {
            const keys = ["MetaLeft", "MetaRight", "ControlLeft", "ControlRight", "AltLeft", "AltRight"];
            const shift = keys.some(key => KeyboardHandler.global.heldKeys.has(key));
            if (!shift) return;
            return largeGrid(x, y, interaction, offset, index);
          },
        ],
        relativePoints: [{ x: 0, y: 0 }],
      }),
    ];
  }
}
