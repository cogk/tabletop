export class KeyboardHandler {
  /** @returns {KeyboardHandler} */
  static get global() {
    if (!KeyboardHandler._global) {
      KeyboardHandler._global = new KeyboardHandler();
    }
    return KeyboardHandler._global;
  }

  constructor() {
    this.heldKeys = new Map();
    this.listeners = {
      up: [],
      down: [],
      held: [],
    };
    this._listen();

    this._loop = this._loop.bind(this);
    this._loop();
  }

  _loop() {
    window.requestAnimationFrame(this._loop);
    if (this.heldKeys.size === 0) {
      return;
    }
    for (const { key, callback } of this.listeners.held) {
      if (this.heldKeys.has(key)) {
        callback(this.heldKeys.get(key), this.heldKeys);
      }
    }
  }

  /**
   * @param {string} key
   * @param {(e: KeyboardEvent) => void} callback
   */
  up(key, callback) {
    this.listeners.up.push({ key, callback });
  }

  down(key, callback) {
    this.listeners.down.push({ key, callback });
  }

  held(key, callback) {
    this.listeners.held.push({ key, callback });
  }

  /**
   * @private
   */
  _listen() {
    document.addEventListener("keydown", this._onKeyDown.bind(this));
    document.addEventListener("keyup", this._onKeyUp.bind(this));
  }

  /**
   * @param {KeyboardEvent} event
   * @private
   */
  _onKeyDown(event) {
    if (event.repeat) {
      return;
    }
    this.heldKeys.set(event.code, event);

    for (const { key, callback } of this.listeners.down) {
      if (key === event.code) {
        callback(event, this.heldKeys);
      }
    }
  }

  /**
   * @param {KeyboardEvent} event
   * @private
   */
  _onKeyUp(event) {
    this.heldKeys.delete(event.code);

    for (const { key, callback } of this.listeners.up) {
      if (key === event.code) {
        callback(event, this.heldKeys);
      }
    }
  }
}
