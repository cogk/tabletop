import { onMounted, onUnmounted } from "vue";
import { uiBus } from "../bus";
import { Gestures } from "./gestures";
import { app } from "../global/app";

export const usePanner = () => {
  onMounted(() => {
    app.panner?.start(document.getElementById("tabletop"));
  });

  onUnmounted(() => {
    app.panner?.stop();
  });
};

export class Panner extends Gestures {
  constructor() {
    super();
    this.ignoreFromPanning = ".interactable,.ui-moveable";
  }

  start(element) {
    super.start(element);
    this.element = element;
    this.root = element.closest("svg");

    Object.assign(this.root.style, {
      "touch-action": "none",
      "user-select": "none",
      "overflow": "hidden",
      "-webkit-user-drag": "none",
      "-webkit-tap-highlight-color": "transparent",
    });

    Object.assign(this.element.style, {
      "transform-origin": "center",
      "transform-box": "view-box",
      "transform": this.getTransform(),
      "will-change": "transform",
    });
  }

  get zoomLevel() {
    return this.getZoomLevel();
  }

  get rotationAngle() {
    return this.getRotation();
  }

  /** @private */ getWorldPos() {
    const { x, y } = this.element.dataset;
    return { x: parseFloat(x) || 0, y: parseFloat(y) || 0 };
  }
  /** @private */ _setWorldPos(x, y) {
    this.element.dataset.x = x;
    this.element.dataset.y = y;
  }
  setWorldPos(x, y) {
    this._setWorldPos(x, y);
    this.update();
  }
  /** @private */ getZoomLevel() {
    return parseFloat(this.element.dataset.zoom) || 1;
  }
  /** @private */ _setZoomLevel(zoomLevel) {
    this.element.dataset.zoom = zoomLevel;
  }
  setZoomLevel(zoomLevel) {
    this._setZoomLevel(zoomLevel);
    this.update();
  }
  /** @private */ getRotation() { // rotation angle in degrees
    return parseFloat(this.element.dataset.angle) || 0;
  }
  /** @private */ _setRotation(rotation) {
    this.element.dataset.angle = rotation;
  }
  setRotation(rotation) {
    this._setRotation(rotation);
    this.update();
  }

  moveBy(dx, dy) {
    const { x, y } = this.getWorldPos();
    this._setWorldPos(x + dx, y + dy);
  }

  rotateBy(da) {
    this._setRotation(this.getRotation() + da);
  }

  zoomBy(dz) {
    let zoomLevel = this.getZoomLevel() * (1 + dz);
    zoomLevel = Math.max(1/4, Math.min(zoomLevel, 32));
    this._setZoomLevel(zoomLevel);
  }

  getRoundedValues() {
    let { x, y } = this.getWorldPos();
    x = Math.round(x);
    y = Math.round(y);

    let zoomLevel = this.getZoomLevel();
    zoomLevel = Math.round(zoomLevel * 32) / 32;

    let rotation = this.getRotation();
    // rotation = Math.round(rotation / 15) * 15;

    return { x, y, zoomLevel, rotation };
  }

  update() {
    this.element.style.transform = this.getTransform();
    uiBus.emitEvent({ type: "ui:camera" });
  }

  getTransform() {
    const { x, y, zoomLevel, rotation } = this.getRoundedValues();
    const { width, height } = this.getViewportSize();
    const center = { x, y };

    const t = {
      scale: zoomLevel,
      rotate: rotation,
      x: -center.x + width / 2,
      y: -center.y + height / 2,
    };

    return `scale(${t.scale}) rotate(${t.rotate}deg) translate(${t.x}px, ${t.y}px)`;
  }

  convertScreenDeltaToWorldDelta(dx, dy) {
    let { zoomLevel, rotation } = this.getRoundedValues();
    // Fix the delta because of the zoom
    zoomLevel = this.getZoomLevel();
    dx /= zoomLevel;
    dy /= zoomLevel;

    // Fix the delta because of the rotation
    rotation = -rotation * (Math.PI / 180);
    const c = Math.cos(rotation);
    const s = Math.sin(rotation);
    [dx, dy] = [dx * c - dy * s, dx * s + dy * c];

    return { dx, dy };
  }

  /** Axis-aligned bounding box of the viewport, in world space */
  getAABB() {
    const { x: cx, y: cy } = this.getWorldPos();
    const zoomLevel = this.getZoomLevel();
    let { width: w0, height: h0 } = this.getViewportSize();
    w0 /= zoomLevel;
    h0 /= zoomLevel;

    const angle = this.getRotation() * (Math.PI / 180);
    const C = Math.cos(angle);
    const S = Math.sin(angle);

    // Three corners of the viewport in world space
    // let [x, y] = [cx - w0 / 2, cy - h0 / 2];
    // [x, y] = [x * C - y * S, x * S + y * C];

    // Get the AA width and height after rotation
    const width = Math.abs(w0 * C) + Math.abs(h0 * S);
    const height = Math.abs(w0 * S) + Math.abs(h0 * C);

    const x = cx - width / 2;
    const y = cy - height / 2;

    return { x, y, width, height };
  }

  _getViewportSize() {
    const { width, height } = this.root.getBoundingClientRect();
    const out = { width, height };
    this._getViewportSize_cache = out;
    this._getViewportSize_cache_time = Date.now();
    return out;
  }

  getViewportSize() {
    if (!this._getViewportSize_cache || Date.now() - this._getViewportSize_cache_time > 1000) {
      return this._getViewportSize();
    }
    return this._getViewportSize_cache;
  }

  transformToScreenSpace(rect) {
    const { x: cx, y: cy } = this.getWorldPos();
    const vp = this.getViewportSize();
    const zoomLevel = this.getZoomLevel();

    const r = { x: 0, y: 0, w: 0, h: 0, ...rect };
    r.x -= cx;
    r.y -= cy;

    const angle = this.getRotation() * (Math.PI / 180);
    const C = Math.cos(angle);
    const S = Math.sin(angle);
    [r.x, r.y] = [r.x * C - r.y * S, r.x * S + r.y * C];

    r.x /= vp.width / zoomLevel;
    r.y /= vp.height / zoomLevel;
    r.w /= vp.width / zoomLevel;
    r.h /= vp.height / zoomLevel;

    r.x *= vp.width;
    r.y *= vp.height;
    r.w *= vp.width;
    r.h *= vp.height;

    r.x += vp.width / 2;
    r.y += vp.height / 2;

    return r;
  }

  transformToWorldSpace(rect) {
    const { x: cx, y: cy } = this.getWorldPos();
    const vp = this.getViewportSize();
    const zoomLevel = this.getZoomLevel();

    const r = { x: 0, y: 0, w: 0, h: 0, ...rect };
    r.x -= vp.width / 2;
    r.y -= vp.height / 2;

    r.x /= vp.width;
    r.y /= vp.height;
    r.w /= vp.width;
    r.h /= vp.height;

    r.x *= vp.width / zoomLevel;
    r.y *= vp.height / zoomLevel;
    r.w *= vp.width / zoomLevel;
    r.h *= vp.height / zoomLevel;

    const angle = -this.getRotation() * (Math.PI / 180);
    const C = Math.cos(angle);
    const S = Math.sin(angle);
    [r.x, r.y] = [r.x * C - r.y * S, r.x * S + r.y * C];

    r.x += cx;
    r.y += cy;

    return r;
  }
}
