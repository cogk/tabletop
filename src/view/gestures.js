/* eslint-disable no-unused-vars */
import interact from "interactjs";

export class Gestures {
  constructor() {
    this.ignoreFromPanning = null;
    this.ignoreFromZooming = null;
  }

  /**
   * @param {number} dx
   * @param {number} dy
   */
  moveBy(dx, dy) {
    throw new Error("Method must be implemented in subclass of Gestures");
  }

  /**
   * @param {number} da
   */
  rotateBy(da) {
    throw new Error("Method must be implemented in subclass of Gestures");
  }

  /**
   * @param {number} dz
   */
  zoomBy(dz) {
    throw new Error("Method must be implemented in subclass of Gestures");
  }

  update() {
    throw new Error("Method must be implemented in subclass of Gestures");
  }

  start(element) {
    // Panning (desktop and mobile)
    this.i1 = interact(element).draggable({
      inertia: true,
      ignoreFrom: this.ignoreFromPanning,
      onmove: (event) => {
        if (event.shiftKey) return;
        const { dx, dy } = this.convertScreenDeltaToWorldDelta(event.dx, event.dy);
        this.moveBy(-dx, -dy);
        this.update();
      },
      modifiers: [
        interact.modifiers.snap({
          targets: [
            interact.createSnapGrid({ x: 1, y: 1 }),
          ],
          range: Infinity,
          relativePoints: [{ x: 0, y: 0 }],
        }),
      ],
    });

    // Zooming (pinch on mobile)
    // Rotating (two-finger rotate on mobile)
    this.i2 = interact(element).gesturable({
      ignoreFrom: this.ignoreFromZooming,
      onmove: (event) => {
        this.zoomBy(event.ds);
        this.rotateBy(event.da);
        this.update();
      },
    });

    // Zooming (wheel on desktop)
    // Rotating (cmd + wheel on desktop)
    this.i3 = interact(element).on("mousewheel", (event) => {
      if (event.target.closest(this.ignoreFromZooming)) return;
      if (event.ctrlKey || event.metaKey) {
        this.rotateBy(0.1 * (event.deltaX || event.deltaY));
      } else {
        this.zoomBy(-0.001 * (event.deltaX || event.deltaY));
      }
      this.update();
    });
  }

  stop() {
    this.i1?.unset();
    this.i1 = null;
    this.i2?.unset();
    this.i2 = null;
    this.i3?.unset();
    this.i3 = null;
  }
}
