export class Btn {
  /**
   * @param {HTMLElement} domElement
   * @param {import("../../global/app").App} app
   */
  constructor(domElement, app) {
    this.app = app;
    this.domElement = domElement;
    domElement.addEventListener("click", this.action.bind(this));
    this.init();
  }
  init() { }
  action() { throw new Error("Not implemented"); }
  enable() { this.domElement.removeAttribute("disabled"); }
  disable() { this.domElement.setAttribute("disabled", "disabled"); }
}
