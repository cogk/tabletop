import { uiBus } from "../../bus";
import { Btn } from "./Btn";

export class BtnHost extends Btn {
  get eventKey() {
    return "ui:btn:host";
  }

  init() {
    document.addEventListener("keydown", (e) => {
      if (e.key === "h" && e.altKey) {
        this.domElement.click();
      }
    });

    uiBus.on("ui:btn:join", ({ data: { state } }) => {
      if (state === "joined") {
        this.setState("cannotHost");
      }
    });

    this.setState("canHost");
  }

  async action() {
    this.setState("waiting");

    let ok = false;
    try {
      await this.app.p2pHost();
      ok = true;
    } catch (e) {
      console.error(e);
    }

    if (ok) {
      this.setState("hosting");
    } else {
      this.setState("hostFailed");
    }
  }

  /**
   * @param {"canHost" | "cannotHost" | "waiting" | "hostFailed" | "hosting"} state
   */
  setState(state) {
    uiBus.emitEvent({ type: this.eventKey, data: { state } });
    switch (state) {
      case "canHost":
        this.domElement.innerText = "Host";
        this.enable();
        break;
      case "cannotHost":
        this.domElement.innerText = "Cannot host";
        this.disable();
        break;
      case "waiting":
        this.domElement.innerText = "Host…";
        this.disable();
        break;
      case "hostFailed":
        this.domElement.innerText = "Error";
        this.disable();
        break;
      case "hosting":
        this.domElement.innerText = "Hosting!";
        this.disable();
        break;
    }
  }
}
