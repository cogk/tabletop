import { uiBus } from "../../bus";
import { Btn } from "./Btn";

export class BtnJoin extends Btn {
  get eventKey() {
    return "ui:btn:join";
  }

  init() {
    document.addEventListener("keydown", (e) => {
      if (e.key === "j" && e.altKey) {
        this.domElement.click();
      }
    });

    uiBus.on("ui:btn:host", ({ data: { state } }) => {
      if (state === "hosting") {
        this.setState("cannotJoin");
      }
    });

    this.setState("canJoin");
  }

  async action() {
    this.setState("waiting");

    let ok = false;
    try {
      await this.app.p2pJoin();
      ok = true;
    } catch (e) {
      console.error(e);
    }

    if (ok) {
      this.setState("joined");
    } else {
      this.setState("joinFailed");
    }
  }

  /**
   * @param {"canJoin" | "cannotJoin" | "waiting" | "joinFailed" | "joined"} state
   */
  setState(state) {
    uiBus.emitEvent({ type: this.eventKey, data: { state } });
    switch (state) {
      case "canJoin":
        this.domElement.innerText = "Join";
        this.enable();
        break;
      case "cannotJoin":
        this.domElement.innerText = "Cannot join";
        this.disable();
        break;
      case "waiting":
        this.domElement.innerText = "Joining…";
        this.disable();
        break;
      case "joinFailed":
        this.domElement.innerText = "Error";
        this.disable();
        break;
      case "joined":
        this.domElement.innerText = "Joined!";
        this.disable();
        break;
    }
  }
}
