import { useDocumentVisibility, useEventListener, useThrottleFn } from "@vueuse/core";
import { PerfectCursor } from "perfect-cursors";
import { computed, shallowReactive, watch } from "vue";
import { gameBus } from "../bus";
import { asSingleton, useCamera, watchEventBus } from "../hooks";
import { vec2_round_mut } from "../utils/vec";
import { app } from "../global/app";

const INTERPOLATE = true;
const INTERPOLATION_DELAY = 30;
const IMMEDIATE_MY_POINTER = true;

PerfectCursor.MAX_INTERVAL = INTERPOLATION_DELAY; // NOTE: cannot be subclassed

class PerfectCursors {
  constructor() {
    /** @private */
    this.pointersRef = shallowReactive({
      [app.player.uid]: { x: 0, y: 0 },
    });
    /**
     * @private
     * @type {Record<string, PerfectCursor>}
     */
    this.perfectCursorInstances = {};
  }
  get pointers() {
    return this.pointersRef;
  }
  makeCallback(uid) {
    return ([x, y]) => this.setPoint(uid, x, y);
  }
  setPoint(uid, x = 777, y = 777) {
    this.pointersRef[uid] = { x, y };
  }
  addPoint(uid, x = 555, y = 555) {
    if (!INTERPOLATE) return this.setPoint(uid, x, y);
    this.perfectCursorInstances[uid] ??= new PerfectCursor(this.makeCallback(uid));
    this.perfectCursorInstances[uid].addPoint([x, y]);
  }
  clearPoint(uid) {
    this.perfectCursorInstances[uid]?.dispose();
    delete this.pointersRef[uid];
    delete this.perfectCursorInstances[uid];
  }
}

const interpolator = new PerfectCursors();

function transformToScreenSpace(pos) {
  if (!pos || !app.panner) return pos;
  const { x, y } = app.panner.transformToScreenSpace(pos);
  return { x, y };
}

function transformToWorldSpace(pos) {
  if (!pos || !app.panner) return pos;
  const { x, y } = app.panner.transformToWorldSpace(pos);
  return { x, y };
}

const updatePointer = (uid, { x = 0, y = 0 } = {}) => {
  if (!uid || uid === "[host]") return;
  if (uid === app.player.uid) {
    if (IMMEDIATE_MY_POINTER) return; // setPoint already called before
    interpolator.setPoint(uid, x, y); // don't interpolate my own cursor
  } else {
    interpolator.addPoint(uid, x, y);
  }
};

const hidePointer = (uid) => {
  if (!uid || uid === "[host]") return;
  interpolator.clearPoint(uid);
};

export const useAllPointersScreen = asSingleton(function _useAllPointers(includeMe = false) {
  singletonListen();
  return computed(() => {
    void useCamera().value;
    let entries = Object.entries(interpolator.pointers);
    if (!includeMe) {
      // don't show my cursor
      entries = entries.filter(([uid]) => uid !== app.player.uid);
    }
    entries = entries.map(([uid, pointer]) => {
      return [uid, transformToScreenSpace(pointer)];
    });
    return Object.fromEntries(entries);
  });
});

export const useMyPointer = asSingleton(function _useMyPointer() {
  singletonListen();
  return computed(() => {
    return interpolator.pointers[app.player.uid] ?? { x: 0, y: 0 };
  });
});


// Event listeners, to be instantiated only once per app

let listening = false;
function singletonListen() {
  if (listening) return;
  listening = true;

  const visibility = useDocumentVisibility();

  // Listen to DOM events
  const onMouseMove = (event) => {
    if (visibility.value !== "visible") return;
    const transformed = transformToWorldSpace({ x: event.clientX, y: event.clientY });
    vec2_round_mut(transformed);
    if (IMMEDIATE_MY_POINTER) {
      interpolator.setPoint(app.player.uid, transformed.x, transformed.y);
    }
    emitCursorMove_throttled(transformed);
  };

  const emitCursorMove = (transformed) => {
    gameBus.emitEvent({ type: "game:cursor:move", author: app.player.uid, data: transformed });
  };
  const emitCursorMove_throttled = useThrottleFn(emitCursorMove, 32, false, true);

  useEventListener(
    window,
    "pointermove",
    onMouseMove,
    // useThrottleFn(onMouseMove, 10, true, true),
    { passive: true, capture: true },
  );

  useEventListener(document.body, "pointerleave", () => {
    gameBus.emitEvent({ type: "game:cursor:clear", author: app.player.uid });
  });

  watch(visibility, (value) => {
    if (value === "hidden") {
      gameBus.emitEvent({ type: "game:cursor:clear", author: app.player.uid });
    }
  });

  // Listen to game events
  watchEventBus("net:joined", (event) => {
    updatePointer(event.author);
  });

  watchEventBus("game:cursor:move", (event) => {
    updatePointer(event.author, event.data);
  });

  watchEventBus("game:cursor:clear", (event) => {
    hidePointer(event.author);
  });
}
