import { onMounted, onUnmounted } from "vue";
import { gameBus, uiBus } from "../bus";
import { getItemIdFromElement } from "../utils/dom";
import { app } from "../global/app";

export const useMouseSelectionBox = () => {
  onMounted(() => {
    app.selection?.start(
      document.getElementById("tabletop"),
      document.getElementById("tabletop-selection-box"),
    );
  });

  onUnmounted(() => {
    app.selection?.stop();
  });
};

export class MouseSelect {
  // Grab the IDs of elements of class "interactive" that intersect a rectangle defined by the mouse drag

  constructor() {
    this.padding = 10;
    this.selectedElements = [];

    this._handleMouseDown = this.handleMouseDown.bind(this);
    this._handleMouseUp = this.handleMouseUp.bind(this);
    this._handleMouseMove = this.handleMouseMove.bind(this);
  }

  start(eventRoot, displayElement) {
    this.eventRoot = eventRoot;
    this.displayElement = displayElement;

    this.displayElement.setAttribute("opacity", "0");
    this.displayElement.setAttribute("pointer-events", "none");

    this.mouseDown = false;
    this.mouseDownX = 0;
    this.mouseDownY = 0;
    this.mouseUpX = 0;
    this.mouseUpY = 0;

    this.eventRoot.addEventListener("mousedown", this._handleMouseDown);
    this.eventRoot.addEventListener("mouseup", this._handleMouseUp);
    this.eventRoot.addEventListener("mousemove", this._handleMouseMove);

    gameBus.on("game:spawn", (event) => {
      if (event.__remote) return; // don't run for p2p events
      if (this.selectedElements.length !== 0) return; // don't select new items if we're already selecting something

      const iids = event.data?.items?.map((item) => item.iid);
      iids && this.selectElements(iids); // select newly spawned items
    });
  }

  stop() {
    this.eventRoot.removeEventListener("mousedown", this._handleMouseDown);
    this.eventRoot.removeEventListener("mouseup", this._handleMouseUp);
    this.eventRoot.removeEventListener("mousemove", this._handleMouseMove);
  }

  handleMouseDown(event) {
    const interactable = event.target.closest(".interactable");
    if (!interactable && event.button === 0 && !event.shiftKey) {
      // Clear selection if we click on the background
      this.selectedElements = [];
      this.emitUpdatedSelection();
      return;
    }

    if (interactable && event.button === 2 && this.selectedElements.length <= 1) {
      // Select the element we right-clicked on
      this.selectedElements = [getItemIdFromElement(interactable)];
      this.emitUpdatedSelection();
      return;
    }

    if (interactable && event.button === 0) {
      const thisId = getItemIdFromElement(interactable);
      if (!this.selectedElements.includes(thisId)) {
        // If we click on an element that isn't selected, clear the selection
        this.selectedElements = [];
        this.emitUpdatedSelection();
      }
    }

    if (event.button !== 0) { return; }
    if (!event.shiftKey) { return; }
    this.selectedElements = [];
    this.mouseDown = true;
    this.mouseDownX = event.clientX;
    this.mouseDownY = event.clientY;
    this.mouseUpX = event.clientX;
    this.mouseUpY = event.clientY;
    this.updateDisplay();
  }

  handleMouseUp(event) {
    if (!this.mouseDown) { return; }
    this.mouseDown = false;
    this.mouseUpX = event.clientX;
    this.mouseUpY = event.clientY;
    this.selectMousedElements();
    this.updateDisplay();
  }

  handleMouseMove(event) {
    if (!this.mouseDown) { return; }
    this.mouseUpX = event.clientX;
    this.mouseUpY = event.clientY;
    this.selectMousedElements();
    this.updateDisplay();
  }

  updateDisplay() {
    if (!this.mouseDown) {
      this.displayElement.setAttribute("opacity", "0");
      return;
    }
    const { left, right, top, bottom } = this.getMouseRect();
    this.displayElement.setAttribute("x", left);
    this.displayElement.setAttribute("y", top);
    this.displayElement.setAttribute("width", right - left);
    this.displayElement.setAttribute("height", bottom - top);
    this.displayElement.setAttribute("opacity", "1");
  }

  getMouseRect() {
    const left = Math.min(this.mouseDownX, this.mouseUpX);
    const right = Math.max(this.mouseDownX, this.mouseUpX);
    const top = Math.min(this.mouseDownY, this.mouseUpY);
    const bottom = Math.max(this.mouseDownY, this.mouseUpY);
    return { left, right, top, bottom };
  }

  selectMousedElements() {
    const elements = document.getElementsByClassName("interactable"); // --select
    const selectedElements = [];
    for (let i = 0; i < elements.length; i++) {
      const element = elements[i];
      const rect = element.getBoundingClientRect();
      if (this.rectIntersectsMouseRect(rect)) {
        const iid = getItemIdFromElement(element);
        if (iid !== null) {
          selectedElements.push(iid);
        }
      }
    }

    this.selectElements(selectedElements);
  }

  /** @type {Array<string>} */
  selectElements(selectedElements) {
    this.selectedElements = selectedElements.slice();
    this.emitUpdatedSelection();
  }

  emitUpdatedSelection() {
    uiBus.emitEvent({
      type: "ui:select",
      data: {
        selected: this.selectedElements.slice(),
      },
    });
  }

  rectIntersectsMouseRect(rect) {
    const { left, right, top, bottom } = this.getMouseRect();
    const shrunk = this.shrinkRect(rect, this.padding);
    return !(right < shrunk.left || left > shrunk.right || bottom < shrunk.top || top > shrunk.bottom);
  }

  shrinkRect(rect, padding) {
    if (rect.width < padding * 2 || rect.height < padding * 2) return rect;

    return {
      left: rect.left + padding,
      right: rect.right - padding,
      top: rect.top + padding,
      bottom: rect.bottom - padding,
    };
  }
}
