import type { App } from "../global/app";

import { gameBus } from "../bus";

export class HoverHandler {
  app: App;
  hovered: Set<string>;

  constructor(app: App) {
    this.app = app;
    this.hovered = new Set();
    this.listen();
  }

  static targets = [
    ["item", "iid", "game:hover:item@", "iid"],
    ["zone", "zid", "game:hover:zone@", "zid"],
  ] as const;

  pointerchange(domEvent: PointerEvent) {
    const child = domEvent.target as (HTMLElement | null);
    const target = child?.closest(".i--hoverable") as (HTMLElement | null);
    for (const [xtype, key, eventPrefix, attr] of HoverHandler.targets) {
      const xid = target?.dataset[attr];
      if (!xid) continue;

      const eventName = eventPrefix + xid;
      const state = domEvent.type === "pointerover";

      const event = {
        type: eventName,
        data: { [key]: xid, type: xtype, state },
        uid: this.app.player.uid,
      };

      gameBus.emitEvent(event);
      return;
    }
  }

  listen() {
    // Listen for dom events
    // pointerover hit test
    // pointerenter hit test or descendant
    // pointerout hit test
    // pointerleave hit test or descendant

    // const cb = throttle(this.pointerchange.bind(this), 1, true, true);
    const cb = this.pointerchange.bind(this);
    document.body.addEventListener("pointerover", cb);
    document.body.addEventListener("pointerout", cb);

    // Listen for game events
    // for (const [, key, eventPrefix] of Object.values(ZonesHandler.targets)) {
    //   const regex = new RegExp(`^${eventPrefix}`);
    //   gameBus.onRegex(regex, (event) => {
    //     const xid: string = event.data[key];
    //     console.log(xid);
    //   });
    // }
  }
}




// import type { Ref } from "vue";
// import { onMounted, onUnmounted, ref, watch } from "vue";
// import { useMyPointer } from "./pointers";

// interface Zone {
//   /** Whether the local player is hovering the zone with their cursor. */
//   hoveredByMe: Ref<boolean>;

//   /** List of the IDs of players currently hovering the zone with their cursors. */
//   // hoveredByUids: string[];
// }

// const zones: Record<string, Zone> = {};

// const makeZone = (zoneId: string, force = false) => {
//   if (!force && zones[zoneId]) return;
//   zones[zoneId] = {
//     hoveredByMe: ref(false),
//   };
//   return zones[zoneId];
// };

// export const useZoneInfo = (zoneId: string) => {
//   listenZones();
//   onMounted(() => makeZone(zoneId));
//   onUnmounted(() => { delete zones[zoneId]; });
//   return makeZone(zoneId);
// };

// let listening = false;
// export const listenZones = () => {
//   if (listening) return;
//   listening = true;

//   const myPointer = useMyPointer();

//   watch(myPointer, (pointer) => {
//     for (const zoneId in zones) {
//       const zone = zones[zoneId];
//       zone.hoveredByMe.value = false;
//     }
//   });

//   // const allPointers = useAllPointers(true);
//   // watch(allPointers, (pointers) => {
//   //   for (const uid in pointers) {
//   //     const pointer = pointers[uid];
//   //     const zoneId = pointer.hoveringZoneId;
//   //     if (!zoneId) continue;
//   //     const zone = makeZone(zoneId);
//   //     if (!zone) continue;
//   //     zone.hoveredByMe.value = true;
//   //   }
//   // });
// };
