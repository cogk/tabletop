// This file contains the glue code that connects the UI to the game state.

import { uiBus } from "../bus";
import { getItemIdFromElement } from "../utils/dom";

// import { EffectCantInteractWithItem } from "./effects";

export class EventGlue {
  /**
   * @param {import("../global/app").App} app
   * @param {import("../items/GameState").GameState} state
  */
  constructor(app, state) {
    this.app = app;
    this.state = state;

    uiBus.on("ui:interact:move", ({ iid, to }) => {
      app.emitEventIfAllowed(state.eventFactory.move(iid, to));
    });

    uiBus.on("ui:interact:movemany", ({ iids, positions }) => {
      app.emitEventIfAllowed(state.eventFactory.movemany(iids, positions));
    });

    const lift = ({ element }) => {
      const iid = getItemIdFromElement(element);
      if (iid) {
        app.emitEventIfAllowed(state.eventFactory.lift(iid));
      }
    };
    uiBus.on("ui:interact:down", lift);
    uiBus.on("ui:interact:up", lift);

    uiBus.on("ui:interact:doubletap", ({ element }) => {
      const iid = getItemIdFromElement(element);
      const item = app.getItem(iid);
      if (!item) return console.warn("no local item for", iid);
      if (item.public) {
        app.emitEventIfAllowed(state.eventFactory.flipHide(item.iid));
        // if (item.owners?.length && item.owners.includes(app.player.uid)) {
        //   state.emitEvent(state.eventFactory.flipHide(item.iid));
        // } else {
        //   const effect = new EffectCantInteractWithItem({
        //     iid: item.iid,
        //     message: "cannot flip unowned item",
        //   });
        //   app.effects.showEffect("forbidden", effect);
        // }
      } else {
        app.emitEventIfAllowed(state.eventFactory.flipShow(item.iid));
      }
    });

    // uiBus.on("ui:notify:forbidden", ({ data }) => {
    //   const { message, forbiddenEvent } = data;
    //   const effect = new EffectCantInteractWithItem({ iid: forbiddenEvent.iid, message });
    //   app.effects.showEffect("forbidden", effect);
    // });

    uiBus.onRegex(/^ui:selection:/, (event) => {
      if (event.type === "ui:selection:clear") {
        app.selection.selectElements([]);
      } else if (event.type === "ui:selection:selectAll") {
        const iids = app.state.items.map((it) => it.iid);
        app.selection.selectElements(iids);
      }
    });
  }
}
