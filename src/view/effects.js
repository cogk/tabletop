import { setTransform } from "../utils/dom";

export class BaseEffect {
  constructor() {
    this.outputElement = document.createElementNS("http://www.w3.org/2000/svg", "g");
    this.outputWrapper = document.body;
    this.timer = null;
  }

  later(delay, callback) {
    this.clearTimer();
    this.timer = setTimeout(() => {
      this.timer = null;
      callback();
    }, delay);
  }

  clearTimer() {
    if (this.timer) {
      clearTimeout(this.timer);
      this.timer = null;
    }
  }

  /** @param {EffectsManager} fx */
  show(fx) {
    this.clearTimer();
    if (!this.outputElement.isConnected) {
      this.outputWrapper.appendChild(this.outputElement);
    }
  }

  /** @param {EffectsManager} fx */
  hide(fx) {
    this.clearTimer();
    if (this.outputElement.isConnected) {
      this.outputWrapper.removeChild(this.outputElement);
    }
  }

  /** @param {EffectsManager} fx */
  cancel(fx) {
    this.clearTimer();
    if (this.outputElement.isConnected) {
      this.outputWrapper.removeChild(this.outputElement);
    }
  }

  replaceElement(newEl, html = null) {
    const oldEl = this.outputElement;
    oldEl.parentNode.replaceChild(newEl, oldEl);
    this.outputElement = newEl;
    if (html !== null) {
      newEl.innerHTML = html;
    }
    return newEl;
  }
}

export class EffectCantInteractWithItem extends BaseEffect {
  constructor({ iid, message }) {
    super();
    this.outputWrapper = document.getElementById("effects-svg");

    this.iid = iid;
    this.message = message;
  }

  show(fx) {
    super.show(fx);

    const target = document.getElementById(this.iid);
    if (!target) return this.iid && console.warn("no target for", this.iid);

    const rect = target.getBoundingClientRect();
    const X = Number(target.dataset.x);
    const Y = Number(target.dataset.y);
    const scale = fx.app.panner.zoomLevel;
    const W = rect.width / scale;
    const H = rect.height / scale;
    const pad = 8;

    const el = this.replaceElement(
      document.createElementNS("http://www.w3.org/2000/svg", "svg"),
      `
        <rect width="100%" height="100%" fill="rgba(255, 0, 0, 0.2)" rx="4" ry="4" />
        <line x1="${pad}" y1="${pad}" x2="${W - pad}" y2="${H - pad}" stroke="red" stroke-width="8" stroke-linecap="round" />
      `,
    );

    setTransform(el, X, Y);
    el.setAttribute("width", W);
    el.setAttribute("height", H);
    el.style.pointerEvents = "none";

    this.later(250, () => {
      this.hide();
    });
  }
}


export class EffectsManager {
  constructor(app) {
    this.app = app;

    /** @type {Map<any, BaseEffect>} */
    this.activeEffects = new Map();
  }

  /**
   * @param {any} key
   * @param {BaseEffect} effectInstance
   */
  showEffect(key, effectInstance) {
    if (this.activeEffects.has(key)) {
      const existing = this.activeEffects.get(key);
      existing?.cancel(this);
      this.activeEffects.delete(key);
    }

    this.activeEffects.set(key, effectInstance);
    effectInstance.show(this);
  }
}
