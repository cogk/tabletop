"use strict";

// https://stackoverflow.com/questions/33986976/how-can-i-remove-a-buggy-service-worker-or-implement-a-kill-switch/38980776#38980776
// https://stackoverflow.com/questions/33704791/how-do-i-uninstall-a-service-worker/33705250#33705250
// https://stackoverflow.com/questions/57916788/how-to-update-service-workers/57925710#57925710

self.addEventListener("fetch", (event) => {
  // Get Vite hash from the request URL "https://host:port/path/to/asset-{hash}.ext"
  const hash = event.request.url.match(/-([0-9a-f]{8})\.[a-z]{2,4}$/)?.[1];
  if (hash) {
    event.respondWith(
      caches.open("cached-assets-1").then((cache) => {
        return cache.match(event.request).then((response) => {
          return response || fetch(event.request).then((response) => {
            cache.put(event.request, response.clone());
            return response;
          });
        });
      }),
    );
  }
});
