{
  "root": true,
  "parser": "vue-eslint-parser",
  "parserOptions": {
    "ecmaVersion": 2023,
    "sourceType": "module",
    "parser": "@typescript-eslint/parser"
  },
  "extends": [
    "eslint:recommended",
    "plugin:@typescript-eslint/recommended",
    "plugin:vue/vue3-recommended"
  ],
  "env": {
    "amd": true,
    "node": true,
    "es6": true
  },
  "rules": {
    "semi": "error",
    "vue/max-attributes-per-line": "off",
    "object-curly-spacing": [
      "error",
      "always"
    ],
    "array-bracket-spacing": [
      "error",
      "never"
    ],
    "nonblock-statement-body-position": [
      "error",
      "beside"
    ],
    "no-implicit-coercion": [
      "warn",
      {
        "boolean": false,
        "number": true,
        "string": true
      }
    ],
    "yoda": [
      "error",
      "never",
      {
        "exceptRange": true
      }
    ],
    "indent": [
      "error",
      2,
      {
        "SwitchCase": 1
      }
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "quotes": [
      "error",
      "double"
    ],
    "no-multiple-empty-lines": "off",
    "consistent-return": "off",
    "no-else-return": "off",
    "no-restricted-syntax": "off",
    "class-methods-use-this": "off",
    "no-shadow": "off",
    "no-plusplus": "off",
    "no-use-before-define": "off",
    "no-unused-expressions": "off",
    "default-case": "off",
    "accessor-pairs": "warn",
    "comma-dangle": [
      "error",
      "always-multiline"
    ],
    "eqeqeq": [
      "error",
      "always"
    ],
    "no-loop-func": "error",
    "keyword-spacing": [
      "error",
      {
        "before": true,
        "after": true
      }
    ],
    "space-before-function-paren": [
      "error",
      {
        "anonymous": "always",
        "named": "never"
      }
    ],
    "no-multi-spaces": "error",
    "space-in-parens": [
      "error",
      "never"
    ],
  }
}
